/* eslint-disable */
/* angular-svg-round-progressbar@0.4.8 2016-09-18
 * Modified by Luis Urrutia 2017-03-21 */
(function(){
  "use strict";
// shim layer with setTimeout fallback
// credit Erik Möller and http://www.paulirish.com/2011/requestanimationframe-for-smart-animating/
  (function() {
    var lastTime = 0;
    var vendors = ['webkit', 'moz'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
      window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
      window.cancelAnimationFrame =
        window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame){
      window.requestAnimationFrame = function(callback) {
        var currTime = new Date().getTime();
        var timeToCall = Math.max(0, 16 - (currTime - lastTime));
        var id = window.setTimeout(function() { callback(currTime + timeToCall); },
          timeToCall);
        lastTime = currTime + timeToCall;
        return id;
      };
    }

    if (!window.cancelAnimationFrame){
      window.cancelAnimationFrame = function(id) {
        window.clearTimeout(id);
      };
    }
  }());

  angular.module('angular-svg-round-progressbar', []);

  angular.module('angular-svg-round-progressbar').constant('roundProgressConfig', {
    max:            50,
    semi:           false,
    rounded:        false,
    responsive:     false,
    clockwise:      true,
    radius:         100,
    color:          '#45ccce',
    bgcolor:        '#eaeaea',
    stroke:         15,
    duration:       800,
    animation:      'easeOutCubic',
    animationDelay: 0,
    offset:         0
  });

  angular.module('angular-svg-round-progressbar').service('roundProgressService', ['$window', function($window){
    var service = {};
    var isNumber = angular.isNumber;
    var base = document.head.querySelector('base');

    // credits to http://modernizr.com/ for the feature test
    service.isSupported = !!(document.createElementNS && document.createElementNS('http://www.w3.org/2000/svg', "svg").createSVGRect);

    // fixes issues if the document has a <base> element
    service.resolveColor = base && base.href ? function(value){
        var hashIndex = value.indexOf('#');

        if(hashIndex > -1 && value.indexOf('url') > -1){
          return value.slice(0, hashIndex) + window.location.href + value.slice(hashIndex);
        }

        return value;
      } : function(value){
        return value;
      };

    // deals with floats passed as strings
    service.toNumber = function(value){
      return isNumber(value) ? value : parseFloat((value + '').replace(',', '.'));
    };

    service.getOffset = function(scope, options){
      var value = +options.offset || 0;

      if(options.offset === 'inherit'){
        var parent = scope.$parent;

        while(parent){
          if(parent.hasOwnProperty('$$getRoundProgressOptions')){
            var opts = parent.$$getRoundProgressOptions();
            value += ((+opts.offset || 0) + (+opts.stroke || 0));
          }

          parent = parent.$parent;
        }
      }

      return value;
    };

    service.getTimestamp = ($window.performance && $window.performance.now && angular.isNumber($window.performance.now())) ? function(){
        return $window.performance.now();
      } : function(){
        return new $window.Date().getTime();
      };

    // credit to http://stackoverflow.com/questions/5736398/how-to-calculate-the-svg-path-for-an-arc-of-a-circle
    service.updateState = function(current, total, pathRadius, element, elementRadius, isSemicircle) {
      if(!elementRadius) return element;

      var value       = current > 0 ? Math.min(current, total) : 0;
      var type        = isSemicircle ? 180 : 359.9999;
      var perc        = total === 0 ? 0 : (value / total) * type;
      var start       = polarToCartesian(elementRadius, elementRadius, pathRadius, perc);
      var end         = polarToCartesian(elementRadius, elementRadius, pathRadius, 0);
      var arcSweep    = (perc <= 180 ? 0 : 1);
      var d           = 'M ' + start + ' A ' + pathRadius + ' ' + pathRadius + ' 0 ' + arcSweep + ' 0 ' + end;

      return element.attr('d', d);
    };

    // Easing functions by Robert Penner
    // Source: http://www.robertpenner.com/easing/
    // License: http://www.robertpenner.com/easing_terms_of_use.html

    service.animations = {

      // t: is the current time (or position) of the tween. This can be seconds or frames, steps, seconds, ms, whatever – as long as the unit is the same as is used for the total time.
      // b: is the beginning value of the property.
      // c: is the change between the beginning and destination value of the property.
      // d: is the total time of the tween.
      // jshint eqeqeq: false, -W041: true

      linearEase: function(t, b, c, d) {
        return c * t / d + b;
      },

      easeInQuad: function (t, b, c, d) {
        return c*(t/=d)*t + b;
      },

      easeOutQuad: function (t, b, c, d) {
        return -c *(t/=d)*(t-2) + b;
      },

      easeInOutQuad: function (t, b, c, d) {
        if ((t/=d/2) < 1) return c/2*t*t + b;
        return -c/2 * ((--t)*(t-2) - 1) + b;
      },

      easeInCubic: function (t, b, c, d) {
        return c*(t/=d)*t*t + b;
      },

      easeOutCubic: function (t, b, c, d) {
        return c*((t=t/d-1)*t*t + 1) + b;
      },

      easeInOutCubic: function (t, b, c, d) {
        if ((t/=d/2) < 1) return c/2*t*t*t + b;
        return c/2*((t-=2)*t*t + 2) + b;
      },

      easeInQuart: function (t, b, c, d) {
        return c*(t/=d)*t*t*t + b;
      },

      easeOutQuart: function (t, b, c, d) {
        return -c * ((t=t/d-1)*t*t*t - 1) + b;
      },

      easeInOutQuart: function (t, b, c, d) {
        if ((t/=d/2) < 1) return c/2*t*t*t*t + b;
        return -c/2 * ((t-=2)*t*t*t - 2) + b;
      },

      easeInQuint: function (t, b, c, d) {
        return c*(t/=d)*t*t*t*t + b;
      },

      easeOutQuint: function (t, b, c, d) {
        return c*((t=t/d-1)*t*t*t*t + 1) + b;
      },

      easeInOutQuint: function (t, b, c, d) {
        if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
        return c/2*((t-=2)*t*t*t*t + 2) + b;
      },

      easeInSine: function (t, b, c, d) {
        return -c * Math.cos(t/d * (Math.PI/2)) + c + b;
      },

      easeOutSine: function (t, b, c, d) {
        return c * Math.sin(t/d * (Math.PI/2)) + b;
      },

      easeInOutSine: function (t, b, c, d) {
        return -c/2 * (Math.cos(Math.PI*t/d) - 1) + b;
      },

      easeInExpo: function (t, b, c, d) {
        return (t==0) ? b : c * Math.pow(2, 10 * (t/d - 1)) + b;
      },

      easeOutExpo: function (t, b, c, d) {
        return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
      },

      easeInOutExpo: function (t, b, c, d) {
        if (t==0) return b;
        if (t==d) return b+c;
        if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
        return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
      },

      easeInCirc: function (t, b, c, d) {
        return -c * (Math.sqrt(1 - (t/=d)*t) - 1) + b;
      },

      easeOutCirc: function (t, b, c, d) {
        return c * Math.sqrt(1 - (t=t/d-1)*t) + b;
      },

      easeInOutCirc: function (t, b, c, d) {
        if ((t/=d/2) < 1) return -c/2 * (Math.sqrt(1 - t*t) - 1) + b;
        return c/2 * (Math.sqrt(1 - (t-=2)*t) + 1) + b;
      },

      easeInElastic: function (t, b, c, d) {
        var s=1.70158;var p=0;var a=c;
        if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*0.3;
        if (a < Math.abs(c)) { a=c; s=p/4; }
        else s = p/(2*Math.PI) * Math.asin (c/a);
        return -(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
      },

      easeOutElastic: function (t, b, c, d) {
        var s=1.70158;var p=0;var a=c;
        if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*0.3;
        if (a < Math.abs(c)) { a=c; s=p/4; }
        else s = p/(2*Math.PI) * Math.asin (c/a);
        return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
      },

      easeInOutElastic: function (t, b, c, d) {
        // jshint eqeqeq: false, -W041: true
        var s=1.70158;var p=0;var a=c;
        if (t==0) return b;  if ((t/=d/2)==2) return b+c;  if (!p) p=d*(0.3*1.5);
        if (a < Math.abs(c)) { a=c; s=p/4; }
        else s = p/(2*Math.PI) * Math.asin (c/a);
        if (t < 1) return -0.5*(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
        return a*Math.pow(2,-10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )*0.5 + c + b;
      },

      easeInBack: function (t, b, c, d, s) {
        // jshint eqeqeq: false, -W041: true
        if (s == undefined) s = 1.70158;
        return c*(t/=d)*t*((s+1)*t - s) + b;
      },

      easeOutBack: function (t, b, c, d, s) {
        // jshint eqeqeq: false, -W041: true
        if (s == undefined) s = 1.70158;
        return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
      },

      easeInOutBack: function (t, b, c, d, s) {
        // jshint eqeqeq: false, -W041: true
        if (s == undefined) s = 1.70158;
        if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
        return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
      },

      easeInBounce: function (t, b, c, d) {
        return c - service.animations.easeOutBounce (d-t, 0, c, d) + b;
      },

      easeOutBounce: function (t, b, c, d) {
        if ((t/=d) < (1/2.75)) {
          return c*(7.5625*t*t) + b;
        } else if (t < (2/2.75)) {
          return c*(7.5625*(t-=(1.5/2.75))*t + 0.75) + b;
        } else if (t < (2.5/2.75)) {
          return c*(7.5625*(t-=(2.25/2.75))*t + 0.9375) + b;
        } else {
          return c*(7.5625*(t-=(2.625/2.75))*t + 0.984375) + b;
        }
      },

      easeInOutBounce: function (t, b, c, d) {
        if (t < d/2) return service.animations.easeInBounce (t*2, 0, c, d) * 0.5 + b;
        return service.animations.easeOutBounce (t*2-d, 0, c, d) * 0.5 + c*0.5 + b;
      }
    };

    // utility function
    function polarToCartesian(centerX, centerY, radius, angleInDegrees) {
      var angleInRadians = (angleInDegrees - 90) * Math.PI / 180.0;
      var x = centerX + (radius * Math.cos(angleInRadians));
      var y = centerY + (radius * Math.sin(angleInRadians));

      return x + ' ' + y;
    }

    return service;
  }]);

  angular.module('angular-svg-round-progressbar').directive('roundProgress', ['$window', 'roundProgressService', 'roundProgressConfig', function($window, service, roundProgressConfig){
    var base = {
      restrict: 'EA',
      replace: true,
      transclude: true,
      scope: {
        current:        '=',
        max:            '=',
        semi:           '=',
        rounded:        '=',
        clockwise:      '=',
        responsive:     '=',
        onRender:       '=',
        radius:         '@',
        color:          '@',
        bgcolor:        '@',
        stroke:         '@',
        backstroke:     '@',
        duration:       '@',
        animation:      '@',
        offset:         '@',
        animationDelay: '@'
      }
    };

    if(!service.isSupported){
      return angular.extend(base, {
        // placeholder element to keep the structure
        template: '<div class="round-progress" ng-transclude></div>'
      });
    }

    return angular.extend(base, {
      link: function(scope, element, attrs){
        var isNested    = !element.hasClass('round-progress-wrapper');
        var svg         = isNested ? element : element.find('svg').eq(0);
        var ring        = svg.find('path').eq(0);
        var background  = svg.find('circle').eq(0);
        var options     = angular.copy(roundProgressConfig);
        var lastAnimationId = 0;
        var lastTimeoutId;
        var parentChangedListener;

        scope.$$getRoundProgressOptions = function(){
          return options;
        };

        var renderCircle = function(){
          var isSemicircle     = options.semi;
          var responsive       = options.responsive;
          var radius           = +options.radius || 0;
          var stroke           = +options.stroke;
          var backStroke       = +options.backstroke;
          var diameter         = radius*2;
          var backgroundSize   = radius - (stroke/2) - service.getOffset(scope, options);

          svg.css({
            top:          0,
            left:         0,
            position:     responsive ? 'absolute' : 'static',
            width:        responsive ? '100%' : (diameter + 'px'),
            height:       responsive ? '100%' : (isSemicircle ? radius : diameter) + 'px',
            overflow:     'hidden' // on some browsers the background overflows, if in semicircle mode
          });

          // when nested, the element shouldn't define its own viewBox
          if(!isNested){
            // note that we can't use .attr, because if jQuery is loaded,
            // it lowercases all attributes and viewBox is case-sensitive
            svg[0].setAttribute('viewBox', '0 0 ' + diameter + ' ' + (isSemicircle ? radius : diameter));
          }

          element.css({
            width:           responsive ? '100%' : 'auto',
            position:        'relative',
            paddingBottom:   responsive ? (isSemicircle ? '50%' : '100%') : 0
          });

          ring.css({
            stroke:          service.resolveColor(options.color),
            strokeWidth:     stroke,
            strokeLinecap:   options.rounded ? 'round': 'butt'
          });

          if(isSemicircle){
            ring.attr('transform', options.clockwise ? 'translate(0, ' + diameter + ') rotate(-90)' : 'translate(' + diameter + ', '+ diameter +') rotate(90) scale(-1, 1)');
          }else{
            ring.attr('transform', options.clockwise ? '' : 'scale(-1, 1) translate(' + (-diameter) + ' 0)');
          }

          background.attr({
            cx:           radius,
            cy:           radius,
            r:            backgroundSize >= 0 ? backgroundSize : 0
          }).css({
            stroke:       service.resolveColor(options.bgcolor),
            strokeWidth:  backStroke
          });
        };

        var renderState = function(newValue, oldValue, preventAnimationOverride){
          var max                 = service.toNumber(options.max || 0);
          var end                 = newValue > 0 ? $window.Math.min(newValue, max) : 0;
          var start               = (oldValue === end || oldValue < 0) ? 0 : (oldValue || 0); // fixes the initial animation
          var changeInValue       = end - start;

          var easingAnimation     = service.animations[options.animation];
          var duration            = +options.duration || 0;
          var preventAnimation    = preventAnimationOverride || (newValue > max && oldValue > max) || (newValue < 0 && oldValue < 0) || duration < 25;

          var radius              = service.toNumber(options.radius);
          var circleSize          = radius - (options.stroke/2) - service.getOffset(scope, options);
          var isSemicircle        = options.semi;

          svg.attr({
            'aria-valuemax': max,
            'aria-valuenow': end
          });

          var doAnimation = function(){
            // stops some expensive animating if the value is above the max or under 0
            if(preventAnimation){
              service.updateState(end, max, circleSize, ring, radius, isSemicircle);

              if(options.onRender){
                options.onRender(end, options, element);
              }
            }else{
              var startTime = service.getTimestamp();
              var id = ++lastAnimationId;

              $window.requestAnimationFrame(function animation(){
                var currentTime = $window.Math.min(service.getTimestamp() - startTime, duration);
                var animateTo = easingAnimation(currentTime, start, changeInValue, duration);

                service.updateState(animateTo, max, circleSize, ring, radius, isSemicircle);

                if(options.onRender){
                  options.onRender(animateTo, options, element);
                }

                if(id === lastAnimationId && currentTime < duration){
                  $window.requestAnimationFrame(animation);
                }
              });
            }
          };

          if(options.animationDelay > 0){
            $window.clearTimeout(lastTimeoutId);
            $window.setTimeout(doAnimation, options.animationDelay);
          }else{
            doAnimation();
          }
        };

        var keys = Object.keys(base.scope).filter(function(key){
          return optionIsSpecified(key) && key !== 'current';
        });

        // properties that are used only for presentation
        scope.$watchGroup(keys, function(newValue){
          for(var i = 0; i < newValue.length; i++){
            if(typeof newValue[i] !== 'undefined'){
              options[keys[i]] = newValue[i];
            }
          }

          renderCircle();
          scope.$broadcast('$parentOffsetChanged');

          // it doesn't have to listen for changes on the parent unless it inherits
          if(options.offset === 'inherit' && !parentChangedListener){
            parentChangedListener = scope.$on('$parentOffsetChanged', function(){
              renderState(scope.current, scope.current, true);
              renderCircle();
            });
          }else if(options.offset !== 'inherit' && parentChangedListener){
            parentChangedListener();
          }
        });

        // properties that are used during animation. some of these overlap with
        // the ones that are used for presentation
        scope.$watchGroup([
          'current',
          'max',
          'radius',
          'stroke',
          'semi',
          'offset'
        ].filter(optionIsSpecified), function(newValue, oldValue){
          renderState(service.toNumber(newValue[0]), service.toNumber(oldValue[0]));
        });

        function optionIsSpecified(name) {
          return attrs.hasOwnProperty(name);
        }
      },
      template: function(element){
        var parent = element.parent();
        var directiveName = 'round-progress';
        var template = [
          '<svg class="'+ directiveName +'" xmlns="http://www.w3.org/2000/svg" role="progressbar" aria-valuemin="0">',
          '<circle fill="none"/>',
          '<path fill="none"/>',
          '<g ng-transclude></g>',
          '</svg>'
        ];

        while(
        parent.length &&
        parent[0].nodeName.toLowerCase() !== directiveName &&
        typeof parent.attr(directiveName) === 'undefined'
          ){
          parent = parent.parent();
        }

        if(!parent || !parent.length){
          template.unshift('<div class="round-progress-wrapper">');
          template.push('</div>');
        }

        return template.join('\n');
      }
    });
  }]);

})();

/* eslint-disable */
'use strict';
angular.module('ngLocale', [], ['$provide', function ($provide) {
  var PLURAL_CATEGORY = {ZERO: 'zero', ONE: 'one', TWO: 'two', FEW: 'few', MANY: 'many', OTHER: 'other'};
  $provide.value('$locale', {
    'DATETIME_FORMATS': {
      'AMPMS': [
        'a. m.',
        'p. m.'
      ],
      'DAY': [
        'Domingo',
        'Lunes',
        'Martes',
        'Mi\u00e9rcoles',
        'Jueves',
        'Viernes',
        'S\u00e1bado'
      ],
      'ERANAMES': [
        'antes de Cristo',
        'despu\u00e9s de Cristo'
      ],
      'ERAS': [
        'a. C.',
        'd. C.'
      ],
      'FIRSTDAYOFWEEK': 0,
      'MONTH': [
        'Enero',
        'Febrero',
        'Marzo',
        'Abril',
        'Mayo',
        'Junio',
        'Julio',
        'Agosto',
        'Septiembre',
        'Octubre',
        'Noviembre',
        'Diciembre'
      ],
      'SHORTDAY': [
        'dom.',
        'lun.',
        'mar.',
        'mi\u00e9.',
        'jue.',
        'vie.',
        's\u00e1b.'
      ],
      'SHORTMONTH': [
        'ene.',
        'feb.',
        'mar.',
        'abr.',
        'may.',
        'jun.',
        'jul.',
        'ago.',
        'sept.',
        'oct.',
        'nov.',
        'dic.'
      ],
      'STANDALONEMONTH': [
        'enero',
        'febrero',
        'marzo',
        'abril',
        'mayo',
        'junio',
        'julio',
        'agosto',
        'septiembre',
        'octubre',
        'noviembre',
        'diciembre'
      ],
      'WEEKENDRANGE': [
        5,
        6
      ],
      'fullDate': "EEEE, d 'de' MMMM 'de' y",
      'longDate': "d 'de' MMMM 'de' y",
      'medium': 'd MMM y H:mm:ss',
      'mediumDate': 'd MMM y',
      'mediumTime': 'H:mm:ss',
      'short': 'd/M/yy H:mm',
      'shortDate': 'd/M/yy',
      'shortTime': 'H:mm'
    },
    'NUMBER_FORMATS': {
      'CURRENCY_SYM': '\u20ac',
      'DECIMAL_SEP': ',',
      'GROUP_SEP': '.',
      'PATTERNS': [
        {
          'gSize': 3,
          'lgSize': 3,
          'maxFrac': 3,
          'minFrac': 0,
          'minInt': 1,
          'negPre': '-',
          'negSuf': '',
          'posPre': '',
          'posSuf': ''
        },
        {
          'gSize': 3,
          'lgSize': 3,
          'maxFrac': 2,
          'minFrac': 2,
          'minInt': 1,
          'negPre': '\u00a0\u00a4',
          'negSuf': '-',
          'posPre': '\u00a0\u00a4',
          'posSuf': ''
        }
      ]
    },
    'id': 'es-es',
    'localeID': 'es_ES',
    'pluralCat': function (n) {
      if (n === 1) {
        return PLURAL_CATEGORY.ONE;
      }
      return PLURAL_CATEGORY.OTHER;
    }
  });
}]);

'use strict';
angular.module('main', [
  'ionic',
  'ngCordova',
  'ui.router',
  'angular-svg-round-progressbar',
  'angularTrix'
])
.config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

  // ROUTING with ui.router
  $ionicConfigProvider.views.transition('none');
  $ionicConfigProvider.views.maxCache(0);
  //$urlRouterProvider.otherwise('/welcome');
  $stateProvider
    // this state is placed in the <ion-nav-view> in the index.html
    .state('main', {
      url: '/main',
      abstract: true,
      templateUrl: 'main/templates/menu.html',
      controller: 'MenuCtrl as menu'
    })
    .state('main.dashboard', {
      url: '/dashboard',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/dashboard.html',
          controller: 'DashboardCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.exito', {
      url: '/exito/{trx:/?.*}',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/exito.html',
          controller: 'ExitoCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.rechazo', {
      url: '/rechazo/{trx:/?.*}',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/rechazo.html',
          controller: 'RechazoCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.profehome', {
      url: '/profesorHome',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/profehome.html',
          controller: 'ProfehomeCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.faq', {
      url: '/faq',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/faq.html',
          controller: 'FaqCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.add', {
      url: '/add',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/add.html',
          controller: 'AddCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.credits', {
      url: '/credits',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/credits.html',
          controller: 'CreditsCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.creditosprofe', {
      url: '/creditosprofe',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/creditosprofe.html',
          controller: 'CreditosprofeCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.search', {
      url: '/search',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/search.html',
          controller: 'SearchCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.results', {
      url: '/results',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/results.html',
          controller: 'ResultsCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.teacher', {
      url: '/teacher',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/teacher.html',
          controller: 'TeacherCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.summary', {
      url: '/summary',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/summary.html',
          controller: 'SummaryCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.schedule', {
      url: '/schedule',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/schedule.html',
          controller: 'ScheduleCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.panic', {
      url: '/panic',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/panic.html',
          controller: 'PanicCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.change', {
      url: '/change',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/change.html',
          controller: 'ChangeCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.profile', {
      url: '/profile',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/profile.html',
          controller: 'ProfileCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.calendar', {
      url: '/calendar',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/calendar.html',
          controller: 'CalendarCtrl as ctrl',
          cache: false
        }
      },
      params: {
        profesor: ''
      }
    })
    .state('main.agenda', {
      url: '/agenda',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/agenda.html',
          controller: 'AgendaCtrl as ctrl',
          cache: false
        }
      },
      params: {
        profesor: ''
      }
    })
    .state('main.history_tutor', {
      url: '/history/tutor',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/history-tutor.html',
          controller: 'HistoryTutorCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.history_teacher', {
      url: '/history/teacher',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/history-teacher.html',
          controller: 'HistoryTeacherCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.request', {
      url: '/request',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/request.html',
          controller: 'RequestCtrl as ctrl',
          cache: false
        }
      },
      params: {
        add: ''
      }
    })
    .state('main.evaluation', {
      url: '/evaluation',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/evaluation.html',
          controller: 'EvaluationCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.control', {
      url: '/control',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/control.html',
          controller: 'ControlCtrl as ctrl',
          cache: false
        }
      }
    });
});

/*! angular-trix - v1.0.0 - 2015-12-09
* https://github.com/sachinchoolur/angular-trix
* Copyright (c) 2015 Sachin; Licensed MIT */
!(function () {'use strict';angular.module('angularTrix', []).directive('angularTrix', function () {return {restrict: 'A', require: 'ngModel', scope: {trixInitialize: '&', trixChange: '&', trixSelectionChange: '&', trixFocus: '&', trixBlur: '&', trixFileAccept: '&', trixAttachmentAdd: '&', trixAttachmentRemove: '&'}, link: function (a, b, c, d) {b.on('trix-initialize', function () {d.$modelValue && b[0].editor.loadHTML(d.$modelValue);}), d.$render = function () {b[0].editor && b[0].editor.loadHTML(d.$modelValue), b.on('trix-change', function () {d.$setViewValue(b.html());});};var e = function (d, e) {b[0].addEventListener(d, function (f) {'trix-file-accept' === d && 'true' === c.preventTrixFileAccept && f.preventDefault(), a[e]({e: f, editor: b[0].editor});});};e('trix-initialize', 'trixInitialize'), e('trix-change', 'trixChange'), e('trix-selection-change', 'trixSelectionChange'), e('trix-focus', 'trixFocus'), e('trix-blur', 'trixBlur'), e('trix-file-accept', 'trixFileAccept'), e('trix-attachment-add', 'trixAttachmentAdd'), e('trix-attachment-remove', 'trixAttachmentRemove');}};});})();

/*! angular-trix - v1.0.0 - 2015-12-09
* https://github.com/sachinchoolur/angular-trix
* Copyright (c) 2015 Sachin; Licensed MIT */
(function () {
  'use strict';
  angular.module('angularTrix', []).directive('angularTrix', function () {
    return {
      restrict: 'A',
      require: 'ngModel',
      scope: {
        trixInitialize: '&',
        trixChange: '&',
        trixSelectionChange: '&',
        trixFocus: '&',
        trixBlur: '&',
        trixFileAccept: '&',
        trixAttachmentAdd: '&',
        trixAttachmentRemove: '&'
      },
      link: function (scope, element, attrs, ngModel) {

        element.on('trix-initialize', function () {
          if (ngModel.$modelValue) {
            element[0].editor.loadHTML(ngModel.$modelValue);
          }
        });

        ngModel.$render = function () {
          if (element[0].editor) {
            element[0].editor.loadHTML(ngModel.$modelValue);
          }

          element.on('trix-change', function () {
            ngModel.$setViewValue(element.html());
          });
        };

        var registerEvents = function (type, method) {
          element[0].addEventListener(type, function (e) {
            if (type === 'trix-file-accept' && attrs.preventTrixFileAccept === 'true') {
              e.preventDefault();
            }

            scope[method]({
              e: e,
              editor: element[0].editor
            });
          });
        };

        registerEvents('trix-initialize', 'trixInitialize');
        registerEvents('trix-change', 'trixChange');
        registerEvents('trix-selection-change', 'trixSelectionChange');
        registerEvents('trix-focus', 'trixFocus');
        registerEvents('trix-blur', 'trixBlur');
        registerEvents('trix-file-accept', 'trixFileAccept');
        registerEvents('trix-attachment-add', 'trixAttachmentAdd');
        registerEvents('trix-attachment-remove', 'trixAttachmentRemove');

      }
    };
  });

})();

'use strict';
angular.module('main')
.controller('TeacherCtrl', function () {

  var vm = this;

  vm.maxRating = 5;
  vm.teacher = {
    id: 1,
    name: 'Juan Perez',
    avatar: 'main/assets/images/person1.png',
    lesson: 'Historia y Geografía',
    university: 'U. de Chile',
    rating: 5
  };

});

'use strict';
angular.module('main')
.controller('SummaryCtrl', function () {

  var vm = this;

  vm.form = {};

});

'use strict';
angular.module('main').controller('SearchCtrl', function ($scope, $ionicModal, $log, $http, $state, $ionicLoading, $rootScope, $cordovaGeolocation, Cfg) {

  $rootScope.comunas = [];
  $rootScope.horarios = [];
  $rootScope.materias = [];
  $scope.total_profes = 0;
  var hoy = new Date();
  $scope.search = { comuna: null, materia: null, horario: null, fecha: null, dia: 0, horarioid: 0 };
  $rootScope.busqueda = { resultados: [], comuna: '', materia: '', horario: '', fecha: null, dia: 0, total: 0, horarioid: 0 };

  $scope.loadMaster = function () {
    $rootScope.showload();
    $http.post(Cfg.rest, {action: 'master'}).then(
        function (response, status, headers, config) {
          $rootScope.hideload();
          $rootScope.materias = response.data.materia;
          $rootScope.comunas = response.data.comuna;
          $rootScope.horarios = response.data.horario;

          /* Posicion de GPS + Precios de mi comuna */
          var posOptions = {timeout: 5000, enableHighAccuracy: false};
          $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
            var uri = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' + position.coords.latitude + ',' + position.coords.longitude + '&sensor=false';
            $rootScope.la_comuna = '';
            $http.get(uri).then(function (res, status, headers, config) {
                //$rootScope.miDireccion = res.data.results[0].formatted_address;
                //$rootScope.miComuna = res.data.results[0].address_components[2].long_name;

              res.data.results[0].address_components.forEach(function (item) {
                for (var q = 0; q < item.types.length ; q++) {
                  if (item.types[q] == 'administrative_area_level_1') {
                      // region
                  }
                  if (item.types[q] == 'administrative_area_level_2') {
                    $rootScope.la_provincia = item.long_name;
                  }
                  if (item.types[q] == 'locality' || item.types[q] == 'administrative_area_level_3') {
                    $rootScope.la_comuna = item.long_name;
                  }
                }
              });

              if ($rootScope.la_comuna == '') {
                $rootScope.la_comuna = $rootScope.la_provincia;
              }

                // buscar comuna en maestra
              var found = 0;
              for (var jk = 0; jk < $rootScope.comunas.length; jk++) {
                if ($rootScope.comunas[jk].com_nombre == $rootScope.la_comuna.toUpperCase()) {
                  var found = 1;
                  $scope.search.comuna = $rootScope.comunas[jk].com_id;
                }
              }

              if (found == 0) {
                $rootScope.err('No hemos podido encontrar profesores en ' + $rootScope.la_comuna + ', por favor selecciona la comuna más cercana');
              }

            }, function () {
              $rootScope.err('No hemos podido cargar tu comuna vía GPS, por favor selecciónala de la lista');
            });
          });


        },
        function () {
          $rootScope.hideload();
          $rootScope.err();
        });
  };

  $scope.loadMaster();

    /*
    $ionicModal.fromTemplateUrl('main/templates/modal/soon.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.modal = modal;
      //$scope.modal.show();
    });
  */

  $scope.updateTotal = function () {
      //console.log($rootScope.comunas.filterObjects("com_id", $scope.search.comuna)); // tambien OK
    $scope.total_profes = $rootScope.comunas.filter(function (a) { return a.com_id == $scope.search.comuna; })[0].total;
  };

  $scope.buscar = function () {
    var hoy = new Date();
    var dif = ( $scope.search.fecha != null ? ((hoy.getTime() - $scope.search.fecha.getTime()) / 1000) : 0);
    if (
          ( $scope.search.comuna == null || $scope.search.comuna == '' ) &&
          ( $scope.search.materia == null || $scope.search.materia == '' ) &&
          ( $scope.search.horario == null || $scope.search.horario == '' )
         ) {
      $rootScope.err('Por favor defina a lo menos 1 parametro de búsqueda');
    }
    else if (dif >= 86400) {
      $rootScope.err('Fecha no puede ser del pasado');
    }
    else {
      $rootScope.showload();

      $http.post(Cfg.rest, {
        action: 'buscar',
        comuna: $scope.search.comuna,
        materia: $scope.search.materia,
        horario: $scope.search.horario,
        fecha: ($scope.search.fecha != null ? $rootScope.formatDate($scope.search.fecha) : '')
      }).then(
          function (response, status, headers, config) {
            $rootScope.hideload();
            $rootScope.busqueda.resultados = response.data.total;

            if ($scope.search.comuna != '' && $scope.search.comuna != null) {
              $rootScope.busqueda.comuna = $rootScope.comunas.filter(function (a) { return a.com_id == $scope.search.comuna; })[0].com_nombre;
            }
            if ($scope.search.materia != '' && $scope.search.materia != null) {
              $rootScope.busqueda.materia = $rootScope.materias.filter(function (a) { return a.id == $scope.search.materia; })[0].valor;
            }
            if ($scope.search.horario != '' && $scope.search.horario != null) {
              $rootScope.busqueda.horarioid = $scope.search.horario;
              $rootScope.busqueda.horario = '' + $rootScope.horarios.filter(function (a) { return a.id == $scope.search.horario; })[0].inicio + ' a ' + $rootScope.horarios.filter(function (a) { return a.id == $scope.search.horario; })[0].fin;
            }
            $rootScope.busqueda.fecha = $scope.search.fecha;
            $rootScope.busqueda.dia = $scope.search.dia;

            if (response.data.total > 0) {
              $rootScope.busqueda.resultados = response.data.rows;
            }
            else {
              $rootScope.busqueda.resultados = [];
            }

            $state.go('main.results');
          },
          function () {
            $rootScope.hideload();
            $rootScope.err();
          });
    }
  };

  var vm = this;

  function nextMonth () {
    vm.calendarDate.setMonth(vm.calendarDate.getMonth() + 1);
    _refreshCalendar();
  }

  function previousMonth () {
    vm.calendarDate.setMonth(vm.calendarDate.getMonth() - 1);
    _refreshCalendar();
  }

  function _refreshCalendar (scheduledDays) {
    if (scheduledDays === undefined) {
      scheduledDays = [];
    }
    vm.calendarArray = _getMonthArray(vm.calendarDate.getMonth(), vm.calendarDate.getFullYear(), scheduledDays);
  }

  function _getMonthArray (month, year, scheduledDays) {
    var finalArray = [];

    if (scheduledDays === undefined) {
      scheduledDays = [];
    }

    var today = new Date();

    var firstDayDate = new Date(year, month, 1);
    var lastDayPreviousMonth = new Date(year, month, 0).getDate();
    var lastDay = new Date(year, month + 1, 0).getDate();

    var len = lastDay + firstDayDate.getDay() - 1;

    var rows = Math.ceil(len / 7);

    for (var i = 0; i < rows; i++) {
      finalArray[i] = [];
    }

    var startFrom = lastDayPreviousMonth - (firstDayDate.getDay() - 1);

    for (i = 1; i < firstDayDate.getDay(); i++) {
      finalArray[0][i - 1] = {
        day: startFrom + i,
        class: 'previous'
      };
    }

    var cssClass = 'current';

    var dayCount = 1;
    var nextMonthCount = false;
    for (var r = 0; r < rows; r++) {
      for (var c = 0; c < 7; c++) {
        if (dayCount > lastDay) {
          dayCount = 1;
          nextMonthCount = true;
          cssClass = 'next';
        }

        if (finalArray[r][c] === undefined) {
          finalArray[r][c] = {
            day: dayCount,
            class: cssClass
          };

          if (scheduledDays.indexOf(dayCount) > -1 && nextMonthCount === false) {
            finalArray[r][c].class = 'today';
          }
          dayCount++;
        }
      }
    }

    return finalArray;
  }

  vm.calendarDate = new Date();
  var sc = []; //[vm.calendarDate.getDate()];
  vm.calendarArray = _getMonthArray(vm.calendarDate.getMonth(), vm.calendarDate.getFullYear(), sc);
  vm.nextMonth = nextMonth;
  vm.previousMonth = previousMonth;

  $scope.loadDia = function (dia, o, xclass) {
    if (xclass != 'previous' && xclass != 'next') {
      var sc = [];
      sc[0] = dia;
      vm.calendarArray = _getMonthArray(vm.calendarDate.getMonth(), vm.calendarDate.getFullYear(), sc);
      var nd = new Date(vm.calendarDate.getFullYear(), vm.calendarDate.getMonth(), dia);
      $scope.search.fecha = nd;
      $scope.search.dia = dia;
    }
  };
  //$scope.loadDia(hoy.getDate(), hoy);

});

'use strict';
angular.module('main')
.controller('ScheduleCtrl', function ($log, $scope) {

  var vm = this;
  vm.info = {};
  vm.slider = null;

  vm.options = {
    loop: false,
    effect: 'slide',
    speed: 500
  };

  vm.nextDate = nextDate;
  vm.previousDate = previousDate;

  function nextDate () {
    if (vm.slider !== null) {
      vm.slider.slideNext();
    }
  }

  function previousDate () {
    if (vm.slider !== null) {
      vm.slider.slidePrev();
    }
  }

  $scope.$on('$ionicSlides.sliderInitialized', function (event, data) {
    vm.slider = data.slider;
  });

});

'use strict';
angular.module('main')
.controller('ResultsCtrl', function ($scope, $ionicModal, $log, $ionicHistory, $http, $state, $ionicLoading, $rootScope, Cfg) {

  $scope.goBack = function () {
    $ionicHistory.goBack();
  };

  $scope.seeCalendar = function (user_id) {
    $state.go('main.calendar', { profesor: user_id });
  };

  $scope.modalProfile = null;
  $scope.verPerfilModal = {};

  $scope.closeOpenedProfile = function () {
    $scope.modalProfile.hide();
    setTimeout(function () { $scope.modalProfile.remove(); }, 800);
  };

  $scope.seeProfile = function (id) {
    $scope.showload();

    $http.post(Cfg.rest, {action: 'getMe', profile: 'profesor', id: id, apoderadoRef: localStorage.getItem('HomeClassID')}).then(function (response) {
      $scope.verPerfilModal = response.data.me;
      console.log($scope.verPerfilModal);
      $scope.hideload();
      $ionicModal.fromTemplateUrl('main/templates/verperfil.html', {
        scope: $scope
      }).then(function (modal) {
        $scope.modalProfile = modal;
        $scope.modalProfile.show();
      });

    });
  };
  /*
  var vm = this;

  vm.maxRating = 5;

  vm.results = [{
    id: 1,
    name: 'Juan Perez',
    avatar: 'main/assets/images/person1.png',
    lesson: 'Historia y Geografía',
    university: 'U. de Chile',
    rating: 5,
    ratingQuantity: 20
  }, {
    id: 2,
    name: 'Rosario Silva',
    avatar: 'main/assets/images/person2.png',
    lesson: 'Matemáticas',
    university: 'U. Católica',
    rating: 3,
    ratingQuantity: 20
  }];
  */

});

'use strict';
angular.module('main')
.controller('RequestCtrl', function ($scope, $ionicModal, $log, $ionicHistory, $http, $state, $stateParams, $ionicLoading, $rootScope, $ionicNavBarDelegate, Cfg) {
  var vm = this;

  $rootScope.cargada = 0;
  $rootScope.tengo_puntos = 0;
  $scope.stParamAdd = $stateParams.add;

  if ($stateParams.add == '1') {

    $rootScope.showload();
    $http.post(Cfg.rest, {action: 'planes', id: localStorage.getItem('HomeClassID')}).then(
      function (response, status, headers, config) {
        $scope.planes = response.data.planes;
        $rootScope.cargada = 1;
        $rootScope.hideload();
      },
      function () {
        $rootScope.hideload();
        $rootScope.err();
      });

  }
  else {

    $rootScope.showload();
    $http.post(Cfg.rest, {action: 'planes', id: localStorage.getItem('HomeClassID')}).then(
      function (response, status, headers, config) {
        $scope.planes = response.data.planes;

        if (response.data.tengo_puntos > 0) {
          $rootScope.tengo_puntos = 1;
          $rootScope.confirmar('Tienes ' + response.data.tengo_puntos + ' creditos disponibles, ¿deseas agendar la clase ahora?', function () {

            $http.post(Cfg.rest, {
              action: 'ok_clase',
              user_id: localStorage.getItem('HomeClassID'),
              fecha: $rootScope.comprar.fecha,
              horario: $rootScope.comprar.horario,
              profesor: $rootScope.comprar.profesor,
              alumno: $rootScope.comprar.alumno,
              direccion: $rootScope.direccionAgendar
            }).then(function () {

            });


            $rootScope.ok('Clase agendada con éxito', function () {
              $state.go('main.agenda');
            });
          }, function () {
            $rootScope.ok('Agenda de clase rechazada');
            $state.go('main.agenda');
          });
        }
        else {
          $rootScope.cargada = 1;
        }
        $rootScope.hideload();

      },
      function () {
        $rootScope.hideload();
        $rootScope.err();
      });

  }

  $scope.comprar = function (idplan) {
    $rootScope.showload();
    $http.post(Cfg.rest, {
      action: 'comprar_plan',
      user_id: localStorage.getItem('HomeClassID'),
      plan_id: idplan,
      fecha: ($stateParams.add == '1' ? '' : $rootScope.comprar.fecha),
      horario: ($stateParams.add == '1' ? '' : $rootScope.comprar.horario),
      profesor: ($stateParams.add == '1' ? '' : $rootScope.comprar.profesor),
      alumno: ($stateParams.add == '1' ? '' : $rootScope.comprar.alumno),
      direccion: $rootScope.direccionAgendar
    }).then(
        function (response, status, headers, config) {
          $rootScope.cargada = 1;
          if (response.data.res == 'ok') {


            var f = document.createElement('form');
            f.setAttribute('method', 'post');
            f.setAttribute('action', response.data.url);
            var i = document.createElement('input'); //input element, text
            i.setAttribute('type', 'hidden');
            i.setAttribute('name', 'token_ws');
            i.setAttribute('value', response.data.token);
            f.appendChild(i);
            document.body.appendChild(f);
            f.submit();


          }
          else {
            $rootScope.hideload();
            $rootScope.err(response.data.msg);
          }
        },
        function () {
          $rootScope.hideload();
          $rootScope.err();
        });
  };


    /*vm.requests = [{
      student: 'Juan Perez',
      signature: 'historia y geografía',
      area: 'Las Condes',
      level: '3° Medio',
      schedule: '6 PM - 8 PM'
    }, {
      student: 'Mariana Silva',
      signature: 'QUÍMICA',
      area: 'Providencia',
      level: '4° Medio',
      schedule: '1 PM - 4 PM'
    }, {
      student: 'Marcela Ubeda',
      signature: 'LENGUAJE BÁSICO',
      area: 'Providencia',
      level: '8° Básico',
      schedule: '5 PM - 7 PM'
    }];*/

});

'use strict';
angular.module('main')
  .controller('RechazoCtrl', function ($rootScope, $stateParams, $scope, $state, $http, Cfg) {
    $scope.trx = $stateParams.trx;
  });

'use strict';
angular.module('main')
.controller('ProfileCtrl', function ($scope, $ionicModal, $log, $http, $ionicPopup, $state, $stateParams, $cordovaCamera, $ionicLoading, $rootScope, $ionicNavBarDelegate, Cfg) {
	//$scope.avatar = "main/assets/images/foto-perfil.png";
  $scope.cover = '../../main/assets/images/cover-photo.png';
  $scope.yo = { nombre: '', email: '', childs: [], pass: '', nuevos: [{id: 0, alumno: '', deletable: 0}], bio: '', universidad: 0, intereses: [], direccion: '', especialidad: ''};
  $scope.activeChild = 0;
  $scope.editor = null;
  $rootScope.showload();

  $scope.getBackgroundStyle = function (imagepath) {
    return {
      'background-image': 'url(' + imagepath + ')'
    };
  };

  $http.post(Cfg.rest, {action: 'getMe', id: localStorage.getItem('HomeClassID'), profile: localStorage.getItem('HomeClassProfile')}).then(
	  function (response, status, headers, config) {

	    $scope.yo.nombre = response.data.me.nombre;
	    $scope.yo.email = response.data.me.email;
	    $scope.yo.pass = response.data.me.pass;

    if (response.data.me.hasOwnProperty('avatar')) {
      $scope.avatar = response.data.me.avatar;
    }
    if (response.data.me.hasOwnProperty('file_foto')) {
        // discrimina profesores
      $scope.avatar = response.data.me.file_foto;
      $scope.yo.universidad = response.data.me.universidad_id;
      $scope.yo.especialidad = response.data.me.especialidad;
      $scope.yo.bio = response.data.me.bio;
      var sp = response.data.me.intereses.split(',');
      var o = [];
      for (var a = 0;a < sp.length;a++) {
        o.push({text: sp[a]});
      }
      $scope.yo.intereses = o;
        //$scope.editor.setSelectedRange([0, 0])
        //$scope.editor.insertHTML(response.data.me.bio)
    }
    else {
        // alumno
      $scope.yo.direccion = response.data.me.direccion;
    }
	    $scope.yo.childs = response.data.me.childs;

    $http.post(Cfg.rest, {action: 'master'}).then(
        function (response, status, headers, config) {
          $rootScope.hideload();
          $scope.universidades = response.data.universidades;
        },
        function () {
          $rootScope.hideload();
          $rootScope.err();
        });

	  },
	  function () {
	    $rootScope.hideload();
	    $rootScope.err();
  });

  $scope.setImage = function () {
    $ionicPopup.show({
      template: '',
      title: 'Actualizar imagen',
      subTitle: 'Seleccione origen de imagen',
      scope: $scope,
      buttons: [
        {
          text: '<b>Cámara</b>',
          type: 'button-positive',
          onTap: function (e) {
            $scope.takePicture();
          }
        },
        {
          text: '<b>Galería</b>',
          type: 'button-positive',
          onTap: function (e) {
            $scope.selectPicture();
          }
        },
        {
          text: '<b>Cerrar</b>',
          type: 'button-calm',
          onTap: function (e) {
            return false;
          }
        }

      ]
    });
  };

  $scope.deleteAlumno = function (index) {
    $scope.yo.childs.splice(index, 1);
  };

  $scope.deleteNuevo = function (index) {
    $scope.yo.nuevos.splice(index, 1);
  };


  $scope.addNuevo = function (index) {
    console.log(index);
    if ($scope.yo.nuevos[index].alumno == '') {
      $rootScope.err('Debe ingresar nombre alumno');
    }
    else {
      $scope.yo.nuevos[index].deletable = 1;
      var o = { id: 0, nombre: '', deletable: 0};
      $scope.yo.nuevos.push(o);
    }
  };

  $scope.saveProfile = function () {
    $scope.showload();
    // timeout por tags waiting
    setTimeout(function () {
      var output = [];
      for (var u = 0;u < $scope.yo.intereses.length;u++) {
        output.push($scope.yo.intereses[u].text);
      }
      if (output.length > 0) {
        var interesesString = output.join(',');
      } else {
        var interesesString = '';
      }

      var postData = {
        action: 'updateProfile',
        id: localStorage.getItem('HomeClassID'),
        profile: localStorage.getItem('HomeClassProfile'),
        firstName: $scope.yo.nombre,
        email: $scope.yo.email,
        password: $scope.yo.pass,
        avatar: ($scope.avatar != 'main/assets/images/foto-perfil.png' ? $scope.avatar : ''),
        bio: $scope.yo.bio,
        intereses: interesesString,
        direccion: $scope.yo.direccion,
        universidad: $scope.yo.universidad,
        especialidad: $scope.yo.especialidad
      };

      $rootScope.avatar = ($scope.avatar != 'main/assets/images/foto-perfil.png' ? $scope.avatar : 'main/assets/images/foto-perfil.png');

      $http.post(Cfg.rest, postData)
      .then(function (data, status, headers, config) {
        $scope.hideload();
        $rootScope.ok('Perfil actualizado con éxito');
      });
    }, 1000);
  };

  $scope.selectPicture = function () {
    if (window.cordova) {
      var options = {
        quality: 50,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
        targetWidth: 400,
        targetHeight: 400
      };

      $cordovaCamera.getPicture(options).then(
      function (imageURI) {
        $scope.uploadPicture(imageURI);
      },
      function (err) {
        $ionicLoading.show({template: 'Error al acceder a tu galería', duration: 1500});
      });
    }
    else {
      $scope.uploadPicture('R0lGODlhPQBEAPeoAJosM//AwO/AwHVYZ/z595kzAP/s7P+goOXMv8+fhw/v739/f+8PD98fH/8mJl+fn/9ZWb8/PzWlwv///6wWGbImAPgTEMImIN9gUFCEm/gDALULDN8PAD6atYdCTX9gUNKlj8wZAKUsAOzZz+UMAOsJAP/Z2ccMDA8PD/95eX5NWvsJCOVNQPtfX/8zM8+QePLl38MGBr8JCP+zs9myn/8GBqwpAP/GxgwJCPny78lzYLgjAJ8vAP9fX/+MjMUcAN8zM/9wcM8ZGcATEL+QePdZWf/29uc/P9cmJu9MTDImIN+/r7+/vz8/P8VNQGNugV8AAF9fX8swMNgTAFlDOICAgPNSUnNWSMQ5MBAQEJE3QPIGAM9AQMqGcG9vb6MhJsEdGM8vLx8fH98AANIWAMuQeL8fABkTEPPQ0OM5OSYdGFl5jo+Pj/+pqcsTE78wMFNGQLYmID4dGPvd3UBAQJmTkP+8vH9QUK+vr8ZWSHpzcJMmILdwcLOGcHRQUHxwcK9PT9DQ0O/v70w5MLypoG8wKOuwsP/g4P/Q0IcwKEswKMl8aJ9fX2xjdOtGRs/Pz+Dg4GImIP8gIH0sKEAwKKmTiKZ8aB/f39Wsl+LFt8dgUE9PT5x5aHBwcP+AgP+WltdgYMyZfyywz78AAAAAAAD///8AAP9mZv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAKgALAAAAAA9AEQAAAj/AFEJHEiwoMGDCBMqXMiwocAbBww4nEhxoYkUpzJGrMixogkfGUNqlNixJEIDB0SqHGmyJSojM1bKZOmyop0gM3Oe2liTISKMOoPy7GnwY9CjIYcSRYm0aVKSLmE6nfq05QycVLPuhDrxBlCtYJUqNAq2bNWEBj6ZXRuyxZyDRtqwnXvkhACDV+euTeJm1Ki7A73qNWtFiF+/gA95Gly2CJLDhwEHMOUAAuOpLYDEgBxZ4GRTlC1fDnpkM+fOqD6DDj1aZpITp0dtGCDhr+fVuCu3zlg49ijaokTZTo27uG7Gjn2P+hI8+PDPERoUB318bWbfAJ5sUNFcuGRTYUqV/3ogfXp1rWlMc6awJjiAAd2fm4ogXjz56aypOoIde4OE5u/F9x199dlXnnGiHZWEYbGpsAEA3QXYnHwEFliKAgswgJ8LPeiUXGwedCAKABACCN+EA1pYIIYaFlcDhytd51sGAJbo3onOpajiihlO92KHGaUXGwWjUBChjSPiWJuOO/LYIm4v1tXfE6J4gCSJEZ7YgRYUNrkji9P55sF/ogxw5ZkSqIDaZBV6aSGYq/lGZplndkckZ98xoICbTcIJGQAZcNmdmUc210hs35nCyJ58fgmIKX5RQGOZowxaZwYA+JaoKQwswGijBV4C6SiTUmpphMspJx9unX4KaimjDv9aaXOEBteBqmuuxgEHoLX6Kqx+yXqqBANsgCtit4FWQAEkrNbpq7HSOmtwag5w57GrmlJBASEU18ADjUYb3ADTinIttsgSB1oJFfA63bduimuqKB1keqwUhoCSK374wbujvOSu4QG6UvxBRydcpKsav++Ca6G8A6Pr1x2kVMyHwsVxUALDq/krnrhPSOzXG1lUTIoffqGR7Goi2MAxbv6O2kEG56I7CSlRsEFKFVyovDJoIRTg7sugNRDGqCJzJgcKE0ywc0ELm6KBCCJo8DIPFeCWNGcyqNFE06ToAfV0HBRgxsvLThHn1oddQMrXj5DyAQgjEHSAJMWZwS3HPxT/QMbabI/iBCliMLEJKX2EEkomBAUCxRi42VDADxyTYDVogV+wSChqmKxEKCDAYFDFj4OmwbY7bDGdBhtrnTQYOigeChUmc1K3QTnAUfEgGFgAWt88hKA6aCRIXhxnQ1yg3BCayK44EWdkUQcBByEQChFXfCB776aQsG0BIlQgQgE8qO26X1h8cEUep8ngRBnOy74E9QgRgEAC8SvOfQkh7FDBDmS43PmGoIiKUUEGkMEC/PJHgxw0xH74yx/3XnaYRJgMB8obxQW6kL9QYEJ0FIFgByfIL7/IQAlvQwEpnAC7DtLNJCKUoO/w45c44GwCXiAFB/OXAATQryUxdN4LfFiwgjCNYg+kYMIEFkCKDs6PKAIJouyGWMS1FSKJOMRB/BoIxYJIUXFUxNwoIkEKPAgCBZSQHQ1A2EWDfDEUVLyADj5AChSIQW6gu10bE/JG2VnCZGfo4R4d0sdQoBAHhPjhIB94v/wRoRKQWGRHgrhGSQJxCS+0pCZbEhAAOw==');
    }
  };


  $scope.takePicture = function () {
    var options = {
      quality: 50,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.CAMERA,
      targetWidth: 400,
      targetHeight: 400
    };
    $cordovaCamera.getPicture(options).then(
    function (imageData) {
      $scope.uploadPicture(imageData);
    },
    function (err) {
      $ionicLoading.show({template: 'Error al acceder a tu cámara', duration: 1500});
    });
  };

  $scope.uploadPicture = function (base64) {
    $ionicLoading.show({template: '<ion-spinner></ion-spinner>'});
    var base64Image = 'data:image/jpeg;base64,' + base64;
    var blob = dataURItoBlob(base64Image);
    var objURL = window.URL.createObjectURL(blob);
    var image = new Image();
    image.src = objURL;
    window.URL.revokeObjectURL(objURL);

    var formData = new FormData();
    formData.append('file', blob, 'avataruser.jpg');
    formData.append('upload_preset', 'homeclass');

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4 && xhr.status === 200) {
        var json = JSON.parse(xhr.responseText);
        $scope.avatar = json.url;
        $scope.cover = json.url;
        setTimeout(function () {
          $ionicLoading.hide();
        }, 2000);
      }
    };
    xhr.open('post', 'https://api.cloudinary.com/v1_1/dujuytngk/image/upload');
    xhr.send(formData);
  };

  function dataURItoBlob (dataURI) {

    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
      {byteString = atob(dataURI.split(',')[1]);}
    else
          {byteString = unescape(dataURI.split(',')[1]);}

    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ia], {type: mimeString});
  }

});

'use strict';
angular.module('main')
.controller('ProfehomeCtrl', function ($scope, $log, $http, $state, $ionicLoading, $rootScope, Cfg) {

  $scope.comunas = '';
  $scope.horarios = '';
  $scope.creditos = '';

  $rootScope.showload();

  $http.post(Cfg.rest, {action: 'getProfesorDashboard', id: localStorage.getItem('HomeClassID')}).then(
    function (response, status, headers, config) {

      $scope.comunas = response.data.comunas;
      $scope.horarios = response.data.horarios;
      $scope.creditos = response.data.creditos;

      $rootScope.hideload();
    },
    function () {
      $rootScope.hideload();
      $rootScope.err();
    });


  $scope.gotoMe = function (d) {
    $state.go(d);
  };
});
///

'use strict';
angular.module('main')
.controller('PanicCtrl', function ($log) {

  $log.log('Hello from your Controller: PanicCtrl in module main:. This is your controller:', this);

});

'use strict';
angular.module('main')
.controller('NotifyCtrl', function ($scope, $ionicModal, $log, $ionicHistory, $http, $state, $stateParams, $ionicLoading, $rootScope, $ionicNavBarDelegate, Cfg) {
  var vm = this;

  $rootScope.cargada = 0;
  $rootScope.tengo_puntos = 0;
  $scope.stParamAdd = $stateParams.add;

  if ($stateParams.add == '1') {

    $rootScope.showload();
    $http.post(Cfg.rest, {action: 'planes', id: localStorage.getItem('HomeClassID')}).then(
      function (response, status, headers, config) {
        $scope.planes = response.data.planes;
        $rootScope.cargada = 1;
        $rootScope.hideload();
      },
      function () {
        $rootScope.hideload();
        $rootScope.err();
      });

  }
  else {

    $rootScope.showload();
    $http.post(Cfg.rest, {action: 'planes', id: localStorage.getItem('HomeClassID')}).then(
      function (response, status, headers, config) {
        $scope.planes = response.data.planes;

        if (response.data.tengo_puntos > 0) {
          $rootScope.tengo_puntos = 1;
          $rootScope.confirmar('Tienes ' + response.data.tengo_puntos + ' creditos disponibles, ¿deseas agendar la clase ahora?', function () {

            $http.post(Cfg.rest, {
              action: 'ok_clase',
              user_id: localStorage.getItem('HomeClassID'),
              fecha: $rootScope.comprar.fecha,
              horario: $rootScope.comprar.horario,
              profesor: $rootScope.comprar.profesor
            }).then(function () {

            });


            $rootScope.ok('Clase agendada con éxito', function () {
              $state.go('main.agenda');
            });
          }, function () {
            $rootScope.ok('Agenda de clase rechazada');
            $state.go('main.agenda');
          });
        }
        else {
          $rootScope.cargada = 1;
        }
        $rootScope.hideload();

      },
      function () {
        $rootScope.hideload();
        $rootScope.err();
      });

  }

  $scope.comprar = function (idplan) {
    $rootScope.showload();
    $http.post(Cfg.rest, {
      action: 'comprar_plan',
      user_id: localStorage.getItem('HomeClassID'),
      plan_id: idplan,
      fecha: ($stateParams.add == '1' ? '' : $rootScope.comprar.fecha),
      horario: ($stateParams.add == '1' ? '' : $rootScope.comprar.horario),
      profesor: ($stateParams.add == '1' ? '' : $rootScope.comprar.profesor)
    }).then(
        function (response, status, headers, config) {
          $rootScope.cargada = 1;
          if (response.data.res == 'ok') {


            var f = document.createElement('form');
            f.setAttribute('method', 'post');
            f.setAttribute('action', response.data.url);
            var i = document.createElement('input'); //input element, text
            i.setAttribute('type', 'hidden');
            i.setAttribute('name', 'token_ws');
            i.setAttribute('value', response.data.token);
            f.appendChild(i);
            document.body.appendChild(f);
            f.submit();


          }
          else {
            $rootScope.hideload();
            $rootScope.err(response.data.msg);
          }
        },
        function () {
          $rootScope.hideload();
          $rootScope.err();
        });
  };


    /*vm.requests = [{
      student: 'Juan Perez',
      signature: 'historia y geografía',
      area: 'Las Condes',
      level: '3° Medio',
      schedule: '6 PM - 8 PM'
    }, {
      student: 'Mariana Silva',
      signature: 'QUÍMICA',
      area: 'Providencia',
      level: '4° Medio',
      schedule: '1 PM - 4 PM'
    }, {
      student: 'Marcela Ubeda',
      signature: 'LENGUAJE BÁSICO',
      area: 'Providencia',
      level: '8° Básico',
      schedule: '5 PM - 7 PM'
    }];*/

});

'use strict';
angular.module('main')
.controller('MenuCtrl', function ($http, $ionicModal, $rootScope, $state, $scope, Cfg) {
	$rootScope.soyAlumno = false;
	$rootScope.soyProfesor = false;
	if (localStorage.getItem("HomeClassProfile") == "profesor") {
		$rootScope.soyProfesor = true;
	}
	else {
		$rootScope.soyApoderado = true;
		$rootScope.soyAlumno = true;
	}
	$rootScope.avatar = "main/assets/images/menu/avatar.png";

	$http.post(Cfg.rest, {action: 'getMe', id: localStorage.getItem("HomeClassID"), profile: localStorage.getItem("HomeClassProfile")}).then(
	  function(response, status, headers, config) {
	    $rootScope.me = response.data.me;
	    if (response.data.me && response.data.me.hasOwnProperty("avatar")) {
	      console.log(response.data.me);
	    	$rootScope.avatar = response.data.me.avatar;
	    }
	    if (response.data.me.hasOwnProperty('file_foto')) {
	    	$rootScope.avatar = response.data.me.file_foto;
	    }

  	});

  $scope.closeOpenedNotify = function () {
    $scope.modalNotify.hide();
    setTimeout(function () { $scope.modalNotify.remove(); }, 800);
  };

  $scope.openedNotify = {};
  $scope.modalNotify = null;
  	$scope.openNotify = function (not) {
  		$scope.openedNotify = not;
    $ionicModal.fromTemplateUrl('main/templates/notify.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.modalNotify = modal;
      $scope.modalNotify.show();
      $http.post(Cfg.rest, {action: 'notifyReader', id: not.id});
    });
  	};

  	$scope.notifyOk = function (notid) {
  		$rootScope.showload();
  		$http.post(Cfg.rest, {action: 'notifyOk', id: notid}).then(function () {
	      $http.post(Cfg.rest, {action: 'getNotifications',
	                            id: localStorage.getItem('HomeClassID'),
	                            perfil: localStorage.getItem('HomeClassProfile'),
	                           }).then(
	        function (response, status, headers, config) {
	          $rootScope.qtynotify = response.data.notifyUnread;
	          $rootScope.notify = response.data.notify;
	          $rootScope.minnotify = response.data.notify.length;
	          $rootScope.hideload();
			  $scope.modalNotify.hide();
			  setTimeout(function () { $scope.modalNotify.remove(); }, 300);

	        }
	      );
	    });
  	};

});

'use strict';
angular.module('main')
  .controller('HistoryTutorCtrl', function ($scope, $ionicModal, $log, $http, $state, $stateParams, $ionicLoading, $rootScope, $ionicNavBarDelegate, Cfg) {
    var vm = this;
    $scope.class = [];
    $rootScope.showload();
    $http.post(Cfg.rest, {action: 'getProximasClases', id: localStorage.getItem('HomeClassID')}).then(
    function (response, status, headers, config) {
      $scope.class = response.data.class;
      if (response.data.class) { $scope.clases = response.data.class.length; }

      $rootScope.hideload();

    },
    function () {
      $rootScope.hideload();
      $rootScope.err();
    });

    vm.maxRating = 5;

    vm.toggleGroup = function (id) {
      if (vm.isGroupShown(id)) {
        vm.shownGroup = null;
      } else {
        vm.shownGroup = id;
      }
    };
    vm.isGroupShown = function (id) {
      return vm.shownGroup === id;
    };


  });

'use strict';
angular.module('main')
  .controller('HistoryTeacherCtrl', function () {

    var vm = this;
    vm.histories = [{
      id: 1,
      plan: 'una hora',
      signature: 'historia y geografía',
      details: {
        status: 'Realizado',
        student: 'Juan Perez',
        date: '20 de enero 2017',
        rating: 5,
        percentage: 100,
        pay: '15.000'
      }
    }, {
      id: 2,
      plan: 'plan semestral',
      signature: 'matemáticas',
      details: {
        status: 'Realizado',
        student: 'Juan Perez',
        date: '20 de enero 2017',
        rating: 3,
        percentage: 60,
        pay: '15.000'
      }
    }];

    vm.maxRating = 5;

    vm.toggleGroup = function (id) {
      if (vm.isGroupShown(id)) {
        vm.shownGroup = null;
      } else {
        vm.shownGroup = id;
      }
    };
    vm.isGroupShown = function (id) {
      return vm.shownGroup === id;
    };

  });

'use strict';
angular.module('main')
.controller('FaqCtrl', function ($scope, $log, $http, $state, $ionicLoading, $rootScope, $sce, Cfg) {

  $scope.groups = [];

  $rootScope.showload();

  $http.post(Cfg.rest, {action: 'FAQ'}).then(
    function (response, status, headers, config) {
      $scope.groups = response.data;
      $rootScope.hideload();
    },
    function () {
      $rootScope.hideload();
      $rootScope.err();
    });

  $scope.toggleGroup = function (group) {
    if ($scope.isGroupShown(group)) {
      $scope.shownGroup = null;
    } else {
      $scope.shownGroup = group;
    }
  };
  $scope.html = function (html) {
    return $sce.trustAsHtml(html);
  };
  $scope.isGroupShown = function (group) {
    return $scope.shownGroup === group;
  };
});
///

'use strict';
angular.module('main')
  .controller('ExitoCtrl', function ($rootScope, $stateParams, $scope, $state, $http, Cfg) {
    $scope.trx = $stateParams.trx;
  });

'use strict';
angular.module('main')
.controller('EvaluationCtrl', function ($scope, $ionicModal, $log, $http, $ionicPopup, $state, $stateParams, $ionicLoading, $rootScope, $ionicNavBarDelegate, Cfg) {

  $scope.evaluation = {id: ''};
  $scope.profesores = [];

  $scope.startMe = function () {
    $rootScope.showload();
    $http.post(Cfg.rest, {action: 'clasesPasadoEvaluar', id: localStorage.getItem('HomeClassID')}).then(
    function (response, status, headers, config) {
      $scope.profesores = response.data.profesores;
      $rootScope.hideload();
    },
    function () {
      $rootScope.hideload();
      $rootScope.err();
    });
  };

  $scope.startMe();

  $scope.evaluar = function () {
    if (!vm.hasOwnProperty('question1')) { $rootScope.err('Responda la pregunta 1 por favor'); }
    else if (!vm.hasOwnProperty('question2')) { $rootScope.err('Responda la pregunta 2 por favor'); }
    else if (!vm.hasOwnProperty('question3')) { $rootScope.err('Responda la pregunta 3 por favor'); }
    else {
      $rootScope.confirmar('¿Desea enviar la evaluación? Podrá re-evaluar en próxima clase', function () {
        $rootScope.showload();
        $http.post(Cfg.rest, {action: 'evaluarProfesor',
          evaluador: localStorage.getItem('HomeClassID'),
          clase: $scope.evaluation.id,
          preg1: (vm.question1.yes ? '1' : ''),
          preg2: (vm.question2.yes ? '1' : ''),
          preg3: (vm.question3.yes ? '1' : ''),
        }
                  ).then(
        function (response, status, headers, config) {
          $scope.evaluation.id = '';
          $rootScope.hideload();
          $scope.ok('Gracias por evaluar al profesor', function () {
            $scope.startMe();
          });

        },
        function () {
          $rootScope.hideload();
          $rootScope.err();
        });
      });
    }
  };

  var vm = this;
  vm.yes = yes;
  vm.no = no;

  $scope.evaluable = false;

  function yes (question) {
    _toggle(question, true);
  }

  function no (question) {
    _toggle(question, false);
  }

  function _toggle (question, value) {
    var yes = false;
    var no = false;
    if (value === true) {
      yes = true;
    } else {
      no = true;
    }

    vm[question] = {
      'yes': yes,
      'no': no
    };

  }

});

'use strict';
angular.module('main')
.controller('DashboardCtrl', function () {

  var vm = this;

  // User dashboard
  vm.dashboard = [
    {
      title: 'Busca tu clase',
      icon: 'main/assets/images/dash_search.svg',
      background: 'main/assets/images/block_background1.png',
      color: '#C5DF58'
    }, {
      title: 'Calendario',
      icon: 'main/assets/images/dash_calendar.svg',
      background: 'main/assets/images/block_background2.png',
      color: '#B2D918'
    }, {
      title: 'Historial clases',
      icon: 'main/assets/images/dash_history.svg',
      background: 'main/assets/images/block_background3.png',
      color: '#9FC604'
    }
  ];

  // Tutor dashboard
  vm.dashboard = [
    {
      title: 'Clases disponibles',
      icon: 'main/assets/images/dash_search.svg',
      background: 'main/assets/images/block_background4.png',
      color: '#FBA23B'
    }, {
      title: 'Calendario',
      icon: 'main/assets/images/dash_calendar.svg',
      background: 'main/assets/images/block_background2.png',
      color: '#FF9120'
    }, {
      title: 'Historial clases',
      icon: 'main/assets/images/dash_history.svg',
      background: 'main/assets/images/block_background3.png',
      color: '#F97E02'
    }
  ];

  // User footer
  vm.footer = {
    title: 'Comparte homeclass con tus amigos'
  };

  // Tutor footer
  vm.footer = {
    title: 'Contactanos'
  };

});

'use strict';
angular.module('main')
.controller('CreditsCtrl', function ($scope, $ionicModal, $log, $filter, $http, $state, $stateParams, $ionicLoading, $rootScope, $ionicNavBarDelegate, Cfg) {

  $scope.creditos = 0;
  $rootScope.showload();
  $scope.compras = [];


  $http.post(Cfg.rest, {action: 'getCompras', id: localStorage.getItem('HomeClassID')}).then(
  function (response, status, headers, config) {
    $scope.compras = response.data.compras;

    if ($scope.compras) {
      for (var i = 0; i < $scope.compras.length ; i++) {
        $scope.compras[i].newdate = $rootScope.convertToDate($scope.compras[i].fecha_pago);
          //$scope.compras[i].newdate = $filter('date')(new Date($scope.compras[i].fecha_pago),'yyyy-MM-dd');
      }
    }
    $http.post(Cfg.rest, {action: 'getMe', id: localStorage.getItem('HomeClassID'), profile: localStorage.getItem('HomeClassProfile')}).then(
    function (response, status, headers, config) {
      $scope.creditos = response.data.me.creditos;

      $rootScope.hideload();

    });

  });

  $scope.addcreditos = function () {
    $state.go('main.request', {add: '1'});
  };
});

'use strict';
angular.module('main')
.controller('CreditosprofeCtrl', function ($scope, $ionicModal, $log, $filter, $http, $state, $stateParams, $ionicLoading, $rootScope, $ionicNavBarDelegate, Cfg) {

  $scope.creditos = 0;
  $rootScope.showload();
  $scope.wins = [];


  $http.post(Cfg.rest, {action: 'getGanancias', id: localStorage.getItem('HomeClassID')}).then(
  function (response, status, headers, config) {
    $scope.wins = response.data.wins;
    $rootScope.hideload();
    if ($scope.wins) {
      for (var i = 0; i < $scope.wins.length ; i++) {
        if ($scope.wins[i].clase_realizada == 1) { $scope.creditos++; }
         // $scope.wins[i].newdate = $rootScope.convertToDate($scope.wins[i].fecha_pago);
          //$scope.compras[i].newdate = $filter('date')(new Date($scope.compras[i].fecha_pago),'yyyy-MM-dd');
      }
    }

  });
});

'use strict';
angular.module('main')
.controller('ControlCtrl', function ($scope, $ionicModal, $log, $http, $ionicPopup, $state, $stateParams, $ionicLoading, $rootScope, $ionicNavBarDelegate, Cfg) {

  $scope.control = {profesor: '0', alumno: '0'};
  $scope.profesores = [];
  $scope.alumnos = [];
  $scope.porcentaje = 0;

  $rootScope.showload();

  $scope.buscarAlumno = function () {
  	  $rootScope.showload();
	  $http.post(Cfg.rest, {action: 'alumno_x_profesor', profesor: $scope.control.profesor, id: localStorage.getItem('HomeClassID') }).then(
	  function (response, status, headers, config) {
	    $scope.alumnos = response.data.alumnos;
	    $rootScope.hideload();
	  },
	  function () {
	    $rootScope.hideload();
	    $rootScope.err();
	  });

  };

  $scope.filtrarAlumno = function () {
  	  $scope.porc = false;

  	  if ($scope.control.alumno != '0') {

	  	  $rootScope.showload();
		  $http.post(Cfg.rest, {action: 'porcentaje_avance', profesor: $scope.control.profesor, alumno: $scope.control.alumno, id: localStorage.getItem('HomeClassID')}).then(
		  function (response, status, headers, config) {
    $scope.porcentaje = response.data.porcentaje;
		  	$scope.porc = true;
		    $rootScope.hideload();
		  },
		  function () {
		    $rootScope.hideload();
		    $rootScope.err();
		  });

  	  }


  };

  $http.post(Cfg.rest, {action: 'profesoresPasado', id: localStorage.getItem('HomeClassID')}).then(
  function (response, status, headers, config) {
    $scope.profesores = response.data.profesores;
    $rootScope.hideload();
  },
  function () {
    $rootScope.hideload();
    $rootScope.err();
  });

  $scope.evaluar = function () {
    $rootScope.confirmar('¿Desea enviar la evaluación? Podrá re-evaluar más adelante', function () {
      $rootScope.showload();
      setTimeout(function () {
        $scope.evaluation.id = 0;
        $rootScope.hideload();
        //$scope.ok("Gracias por evaluar al profesor");
      }, 2300);
    });
  };

  $scope.porc = false;

  var vm = this;
  vm.search = {};


  vm.maxProgress = 100;
  vm.studentProgress = 75;

});



'use strict';
angular.module('main')
.controller('ChangeCtrl', function ($scope, $ionicModal, $log, $http, $ionicPopup, $state, $stateParams, $ionicLoading, $rootScope, $ionicNavBarDelegate, Cfg) {

  $scope.cambio = {str: ''};
  $scope.agendas = [];

  $rootScope.showload();

  $http.post(Cfg.rest, {action: 'profesoresFuturo', id: localStorage.getItem('HomeClassID')}).then(
  function (response, status, headers, config) {

    $scope.agendas = response.data.agendas;
    $rootScope.hideload();
  },
  function () {
    $rootScope.hideload();
    $rootScope.err();
  });

  $scope.cambio = function () {
    if ($scope.cambio.str != '') {
      $rootScope.confirmar('Se va a anular la clase y comenzara un nuevo proceso de busqueda, ¿desea continuar?', function () {

        $rootScope.showload();
        $http.post(Cfg.rest, {action: 'anularClase', id: $scope.cambio.str, autor: localStorage.getItem('HomeClassID')}).then(
        function (response, status, headers, config) {

          //$rootScope.preloadBuscar = { resultados: [], comuna: '', materia: '', horario: '', fecha: '', dia: 0, total: 0, horarioid: 0 };
          $state.go('main.search');

          $rootScope.hideload();
        },
        function () {
          $rootScope.hideload();
          $rootScope.err();
        });

      });
    }
  };

  var vm = this;

  vm.maxRating = 5;

  vm.teacher = {
    id: 1,
    name: 'Juan Perez',
    avatar: 'main/assets/images/person1.png',
    lesson: 'Historia y Geografía',
    university: 'U. de Chile',
    rating: 5,
    ratingQuantity: 20
  };

});

'use strict';
angular.module('main')
.controller('CalendarCtrl', function ($scope, $ionicModal, $log, $http, $state, $stateParams, $ionicLoading, $rootScope, $ionicNavBarDelegate, Cfg) {

  var vm = this;

  function nextMonth () {
    vm.calendarDate.setMonth(vm.calendarDate.getMonth() + 1);
    _refreshCalendar();
  }

  function previousMonth () {
    vm.calendarDate.setMonth(vm.calendarDate.getMonth() - 1);
    _refreshCalendar();
  }

  function _refreshCalendar (scheduledDays) {
    if (scheduledDays === undefined) {
      scheduledDays = [];
    }
    vm.calendarArray = _getMonthArray(vm.calendarDate.getMonth(), vm.calendarDate.getFullYear(), scheduledDays);
  }

  function _getMonthArray (month, year, scheduledDays) {
    var finalArray = [];

    if (scheduledDays === undefined) {
      scheduledDays = [];
    }

    var today = new Date();

    var firstDayDate = new Date(year, month, 1);
    var lastDayPreviousMonth = new Date(year, month, 0).getDate();
    var lastDay = new Date(year, month + 1, 0).getDate();

    var len = lastDay + firstDayDate.getDay() - 1;

    var rows = Math.ceil(len / 7);

    for (var i = 0; i < rows; i++) {
      finalArray[i] = [];
    }

    var startFrom = lastDayPreviousMonth - (firstDayDate.getDay() - 1);

    //for (i = 1; i < firstDayDate.getDay(); i++) {
    for (i = 1; i <= today.getDay(); i++) {
      finalArray[0][i - 1] = {
        day: startFrom + i,
        class: 'previous'
      };
    }

    var cssClass = 'current';

    var dayCount = 1;
    var nextMonthCount = false;
    for (var r = 0; r < rows; r++) {
      for (var c = 0; c < 7; c++) {
        if (dayCount > lastDay) {
          dayCount = 1;
          nextMonthCount = true;
          cssClass = 'next';
        }

        if (finalArray[r][c] === undefined) {
          finalArray[r][c] = {
            day: dayCount,
            class: cssClass
          };

          if (scheduledDays.indexOf(dayCount) > -1 && nextMonthCount === false) {
            finalArray[r][c].class = 'today'; //scheduled
          }
          /*
          if (dayCount === today.getDate() && today.getMonth() === month && today.getFullYear() === year) {
            finalArray[r][c].class = 'today';
          }
          */
          dayCount++;
        }
      }
    }

    return finalArray;
  }

  $scope.sel = { horario: '', profesor: '', dia: '', mes: '', ano: '', alumno: '' };
  $scope.profesorView = false;
  $scope.profesor = null;
  $scope.diaSeleccionado = null;
  $scope.mesSeleccionado = null;
  $scope.fechaSeleccionado = new Date();
  $scope.als = [];

  if ($stateParams.profesor != '') {
    $scope.profesorView = true;
    $scope.horarios_dia = [];

    $rootScope.showload();
    $http.post(Cfg.rest, {action: 'getAgenda', profesor: $stateParams.profesor, user_id: localStorage.getItem('HomeClassID')}).then(
      function (response, status, headers, config) {

        /* OJO CON ESTA WEA */
        var scheduledDays = response.data.ocupado;
        $scope.profesor = response.data.profesor;
        $scope.als = response.data.als;
        if (response.data.als != null) { $scope.sel.alumno = response.data.als[0].id; }

        var sc = [];
        if ($rootScope.busqueda.dia != 0) {
          sc = [$rootScope.busqueda.dia];

          vm.calendarDate = $rootScope.busqueda.fecha;
          $scope.fechaSeleccionado = $rootScope.busqueda.fecha;
          $scope.loadDia($rootScope.busqueda.dia, $rootScope.busqueda.fecha);
        }
        else {
          vm.calendarDate = new Date();
        }

        if ($rootScope.busqueda.horario != null && $rootScope.busqueda.horario != '') {
          $scope.sel.horario = $rootScope.busqueda.horarioid;
        }

        $rootScope.direccionAgendar = response.data.direccion;

        $rootScope.hideload();

      },
      function () {
        $rootScope.hideload();
        $rootScope.err();
      });
  }

  $scope.agendar = function () {
    var hoy = new Date();
    var dif = ( (hoy.getTime() - $scope.fechaSeleccionado.getTime()) / 1000);

    if (dif >= 86400) {
      $rootScope.err('Fecha no puede ser del pasado');
    }
    else if ($scope.sel.alumno == '') {
      $rootScope.err('Debe seleccionar alumno para agendar');
    }
    else if ($scope.sel.horario == '') {
      $rootScope.err('Debe seleccionar horario para agendar');
    }
    else {
      $rootScope.comprar = {
        fecha: $rootScope.formatDate($scope.fechaSeleccionado),
        horario: $scope.sel.horario,
        profesor: $stateParams.profesor,
        alumno: $scope.sel.alumno
      };
      $state.go('main.request');
    }
  };
  $scope.loadDia = function (dia, o) {

    $rootScope.showload();

    $http.post(Cfg.rest, {action: 'getDisponibleDia', profesor: $stateParams.profesor, dia: dia, mes: (o.getMonth() + 1), ano: o.getFullYear()}).then(
      function (response, status, headers, config) {

        var sc = [];
        sc[0] = dia;
        //console.log(vm.calendarDate);
        vm.calendarArray = _getMonthArray(vm.calendarDate.getMonth() + 1, vm.calendarDate.getFullYear(), sc);
        var nd = new Date(vm.calendarDate.getFullYear(), vm.calendarDate.getMonth(), dia);
        $scope.fechaSeleccionado = nd;

        if (response.data.disponible == null) {
          $rootScope.hideload();
          $rootScope.err('Uuups!! el profesor no tiene horarios disponibles en la fecha solicitada');
        }
        else {
          $rootScope.hideload();
          $scope.horarios_dia = response.data.disponible;
        }
        $scope.diaSeleccionado = response.data.diaSeleccionado;
        $scope.mesSeleccionado = response.data.mesSeleccionado;


      },
      function () {
        $rootScope.hideload();
        $rootScope.err();
      });
  };

});

'use strict';
angular.module('main')
.controller('AgendaCtrl', function ($scope, $ionicModal, $log, $http, $state, $stateParams, $ionicLoading, $rootScope, $ionicNavBarDelegate, Cfg) {

  var vm = this;
  function nextMonth () { vm.calendarDate.setMonth(vm.calendarDate.getMonth() + 1); }

  function previousMonth () { vm.calendarDate.setMonth(vm.calendarDate.getMonth() - 1); }

  vm.calendarDate = new Date();
  vm.nextMonth = nextMonth;
  vm.previousMonth = previousMonth;

  $scope.nextMes = function () {
    vm.nextMonth();
    $scope.getInfo();
  };

  $scope.prevMes = function () {
    vm.previousMonth();
    $scope.getInfo();
  };
  $scope.sel = { horario: '', profesor: '', dia: '', mes: '', ano: '' };
  $scope.class = [];
  $scope.clases = 0;

  $scope.getInfo = function () {
    $rootScope.showload();

    $http.post(Cfg.rest, {action: 'getAgendaMe',
      id: localStorage.getItem('HomeClassID'),
      perfil: localStorage.getItem('HomeClassProfile'),
      mes: (vm.calendarDate.getMonth() + 1),
      ano: vm.calendarDate.getFullYear()}
    ).then(
      function (response, status, headers, config) {
        $scope.class = response.data.class;
        if (response.data.class) {
          $scope.clases = response.data.class.length;
        }
        else {
          $scope.clases = 0;
        }
        $rootScope.hideload();
      },
      function () {
        $rootScope.hideload();
        $rootScope.err();
      }
    );
  };

  $scope.getInfo();
  $scope.movement = [];
  $scope.modalClase = {};
  $scope.puedeReagendar = 0;
  if (localStorage.getItem('HomeClassProfile') != 'profesor') {
    $scope.puedeReagendar = 1;
  }

  $scope.closeReagendar = function () {
    $scope.seleccionReagendar.hide();
    setTimeout(function () { $scope.seleccionReagendar.remove(); }, 800);
  };
  $scope.reagendar_ok = function (clase, newHorarioId, newFecha) {
    $rootScope.confirmar('¿Confirmas el cambio de horario/dia?', function () {
      $rootScope.showload();
      $http.post(Cfg.rest, {action: 'reAgendarClase',
        clase_id: clase.id,
        horario_id: newHorarioId,
        fecha: newFecha,
        startBy: localStorage.getItem('HomeClassProfile'),
        start: localStorage.getItem('HomeClassID')
      }).then(
          function (response, status, headers, config) {
            $rootScope.hideload();
            $rootScope.ok('Reagendada con éxito', function () {
              $scope.seleccionReagendar.hide();
              setTimeout(function () { $scope.seleccionReagendar.remove(); }, 800);
              $scope.getInfo();
            });
          },
          function () {
            $rootScope.hideload();
            $rootScope.err();
          }
        );
    });

  };
  $scope.profesor_reagendar = function (agenda_id, clase) {
    if (localStorage.getItem('HomeClassProfile') != 'profesor') {

      $rootScope.showload();
      /* ALUMNO MUEVE LA CLASE */
      $http.post(Cfg.rest, {action: 'getDisponibilidadProfesor',
        id: clase.profesor_id,
        perfil: 'profesor',
        reagendar: agenda_id
      }).then(
        function (response, status, headers, config) {
          if (response.data.total == 0) {
            $rootScope.err('El profesor no tiene disponibilidad para mover esta clase');
          }
          else {
            $scope.modalClase = clase;
            $scope.movement = response.data.movement;
            $ionicModal.fromTemplateUrl('main/templates/reagendar.html', {
              scope: $scope
            }).then(function (modal) {
              $scope.seleccionReagendar = modal;
              $scope.seleccionReagendar.show();
            });
          }
          $rootScope.hideload();
        },
        function () {
          $rootScope.hideload();
          $rootScope.err();
        }
      );
    }
    else {

      /* PROFESOR QUIERE MOVER LA CLASE */
      $rootScope.showload();

      $http.post(Cfg.rest, {action: 'getDisponibilidadProfesor',
        id: localStorage.getItem('HomeClassID'),
        perfil: localStorage.getItem('HomeClassProfile'),
        reagendar: agenda_id
      }).then(
        function (response, status, headers, config) {
          $scope.class = response.data.class;
          if (response.data.total == 0) {
            $rootScope.err('No tienes disponibilidad futura para mover esta clase');
          }
          else {
            $scope.modalClase = clase;
            $scope.movement = response.data.movement;
            $ionicModal.fromTemplateUrl('main/templates/reagendar.html', {
              scope: $scope
            }).then(function (modal) {
              $scope.seleccionReagendar = modal;
              $scope.seleccionReagendar.show();
            });
          }
          $rootScope.hideload();
        },
        function () {
          $rootScope.hideload();
          $rootScope.err();
        }
      );

    }

  };
});

'use strict';
angular.module('main')
.controller('AddCtrl', function () {

  var vm = this;

  vm.profile = {
    avatar: 'main/assets/images/foto-perfil.png',
    cover: '../../main/assets/images/cover-photo.png'
  };

});


'use strict';
angular
  .module('main')
  .component('homeclassHeaderProfile', {
    bindings: {
      avatar: '@',
      cover: '@'
    },
    templateUrl: 'main/templates/components/homeclass-header-profile.html',
    controllerAs: 'header'
  });

'use strict';
angular
  .module('main')
  .component('homeclassButton', {
    bindings: {
      text: '@'
    },
    templateUrl: 'main/templates/components/homeclass-button.html',
    controllerAs: 'btn'
  });

'use strict';
angular
  .module('main')
  .component('hamburger', {
    templateUrl: 'main/templates/components/hamburger.html',
    controllerAs: 'btn'
  });

'use strict';
angular
  .module('main')
  .component('bell', {
    templateUrl: 'main/templates/components/bell.html',
    controllerAs: 'btn'
  });

'use strict';
angular.module('auth', [
  'ionic',
  'ngCordova',
  'ui.router',
  // TODO: load other modules selected during generation
])
.config(function ($stateProvider) {

  // ROUTING with ui.router
  $stateProvider
    .state('login', {
      url: '/login',
      templateUrl: 'auth/templates/login.html',
      controller: 'LoginCtrl as ctrl'
    })
    .state('welcome', {
      url: '/welcome',
      templateUrl: 'auth/templates/welcome.html',
      controller: 'WelcomeCtrl as ctrl'
    })
    .state('signup_tutor', {
      url: '/signup/tutor',
      templateUrl: 'auth/templates/signup-tutor.html',
      controller: 'SignupTutorCtrl as ctrl'
    })
    .state('signup_teacher', {
      url: '/signup/teacher',
      templateUrl: 'auth/templates/signup-teacher.html',
      controller: 'SignupTeacherCtrl as ctrl'
    })
  ;
});

'use strict';
angular.module('auth')
.controller('WelcomeCtrl', function ($scope, $log, $http, $state, $ionicLoading, $rootScope, Cfg) {
	//alert('welcomeCTRL');
  $scope.login = {user: '', pass: ''};

  $scope.goLogin = function () {
    $state.go('login');
  };

  $scope.goSignup = function () {
    $state.go('signup_tutor');
  };

});

'use strict';
angular.module('auth')
.controller('SignupTutorCtrl', function ($scope, $log, $http, $state, $ionicLoading, $rootScope, Cfg) {

  $scope.reg = { apoderado: '', alumnos: [ {nombre: ''} ], email: '', clave: '', acepto: false };

  $scope.addAlumno = function () {
    var o = { nombre: '' };
    $scope.reg.alumnos.push(o);
  };

  $scope.registroApoderado = function () {

    if ($scope.reg.apoderado == '') {
      $rootScope.err('Debe ingresar su nombre de apoderado');
    }
    else if ($scope.reg.email == '') {
      $rootScope.err('Debe ingresar un email');
    }
    else if ($scope.reg.clave == '') {
      $rootScope.err('Debe ingresar una clave/contraseña');
    }
    else if ($scope.reg.acepto == false) {
      $rootScope.err('Debe aceptar nuestros términos para crear su cuenta');
    }
    else {
      $rootScope.showload();
      var alumnos = '';
      var alm = [];
      var error_alumno = 1;
      for (var i = 0;i < $scope.reg.alumnos.length;i++) {
        alm.push($scope.reg.alumnos[i].nombre);
        error_alumno = 0;
      }

      if (error_alumno == 0) {
        $http.post(Cfg.rest, {action: 'reg_apo', nombre: $scope.reg.apoderado, alumnos: alm.join(','), email: $scope.reg.email, clave: $scope.reg.clave}).then(
				function (response, status, headers, config) {
  $rootScope.hideload();
  if (response.data.res == 'OK') {
    $rootScope.user_id = response.data.id;
    localStorage.setItem('HomeClassID', $rootScope.user_id);
    $rootScope.ok('Tu cuenta está lista. Bienvenido a HomeClass');
    $state.go('main.search');
  }
  else {
    $rootScope.err(response.data.msg);
  }
},
				function () {
  $rootScope.hideload();
  $rootScope.err();
});
      } else {
        $rootScope.err('Debe ingresar a lo menos 1 alumno');
      }
    }
  };
  $scope.registroGoogle = function () {

	    $scope.showload();
	    window.plugins.googleplus.login(
	        {
	          'scopes': '', // optional, space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
	          'webClientId': '', // optional (client id of the web app/server side) clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
	          'offline': false, // optional, but requires the webClientId - if set to true the plugin will also return a serverAuthCode, which can be used to grant offline access to a non-Google server
	        },
	        function (obj) {

          var dataPost = {
            'username': obj.email,
            'email': obj.email,
            'googleId': obj.userId,
            'avatar': obj.imageUrl,
            'firstName': obj.displayName,
            'social': 1
          };

          $scope.reg.apoderado = obj.displayName;
          $scope.reg.email = obj.email;
          $scope.hideload();
        }, function (err) {
          $rootScope.err(err);
          $scope.hideload();
        });
	    $scope.showload();
  };

  $scope.registroFacebook = function () {

	   $rootScope.showload();

	    CordovaFacebook.login({
	       permissions: ['email', 'public_profile'],
	       onSuccess: function (result) {
	          if (result.declined.length > 0) {
	             $rootScope.err('Se ha rechazado la conexión con Facebook');
	             $rootScope.hideload();
	          }
	          else if (result.success == 1) {

	            $http.get('https://graph.facebook.com/me?fields=id,name,email,picture&access_token=' + result.accessToken, {})
	            .then(function (data, status, headers, config) {

	                var dataPost = {
	                  'username': data.data.email,
	                  'firstName': data.data.name,
	                  'email': data.data.email,
	                  'facebookId': data.data.id,
	                  'picture': data.data.picture.data.url
	                };

	                $scope.reg.apoderado = data.data.name;
              $scope.reg.email = data.data.email;
              $rootScope.hideload();

	            }, function () {
	              $rootScope.hideload();
	              $rootScope.err('No fue posible obtener la información desde Facebook');
	            });

	          }
	          else {
	            $rootScope.hideload();
	            $rootScope.err('Error al intentar conectar con Facebook. Intente más tarde');
	          }
	       },
	       onFailure: function (result) {
	          $rootScope.hideload();
	          if (result.cancelled) {
	             $rootScope.err('No has autorizado Homeclass en tu cuenta de Facebook');
	          } else if (result.error) {
	             $rootScope.err('Bloqueaste a Homeclass para usar tu Facebook. Debes usar otro método de autentificación (' + result.errorLocalized + ')');
	          }
	       }
	    });


  };
});

'use strict';
angular.module('auth')
.controller('SignupTeacherCtrl', function ($scope, $log, $http, $state, $ionicLoading, $rootScope, Cfg) {

  $scope.reg = { lugarestudio: '', empleo: '', area: '', horario: '', pass: '', nombre: '' };

  $scope.registrar = function () {
    if ($scope.reg.lugarestudio == '') {
      $rootScope.err('Debe ingresar su nombre de apoderado');
    }
    else if ($scope.reg.horario == '') {
      $rootScope.err('Debe ingresar al menos 1 bloque horario general de disponibilidad');
    }
    else if ($scope.reg.area == '') {
      $rootScope.err('Debe ingresar al menos 1 area de estudio');
    }
    else if ($scope.reg.pass == '') {
      $rootScope.err('Debe ingresar una clave/contraseña');
    }
    else if ($scope.reg.nombre == '') {
      $rootScope.err('Debe ingresar su nombre');
    }
    else {
      $rootScope.showload();
      $http.post(Cfg.rest, {action: 'reg_profe', nombre: $scope.reg.user, clave: $scope.reg.clave}).then(
				function (response, status, headers, config) {
  $rootScope.hideload();
  if (response.data.res == 'OK') {
    $rootScope.user_id = response.data.id;
    localStorage.setItem('HomeClassID', $rootScope.user_id);
    $rootScope.ok('Tu cuenta está lista. Bienvenido a HomeClass');
    $state.go('main.profile');
  }
  else {
    $rootScope.err(response.data.msg);
  }
},
				function () {
  $rootScope.hideload();
  $rootScope.err();
});
    }
  };

});


'use strict';
angular.module('auth')
.controller('LoginCtrl', function ($scope, $log, $http, $state, $ionicLoading, $rootScope, Cfg) {
	//alert('welcomeCTRL');
  $scope.login = {user: '', pass: ''};

  $scope.goLogin = function () {

	  /* hide onboarding */
    localStorage.setItem('onBoarding', 'true');

    if ($scope.login.user == '') {
      $rootScope.err('Debe ingresar un nombre de usuario');
    }
    else if ($scope.login.pass == '') {
      $rootScope.err('Debe ingresar una contraseña');
    }
    else {
      $rootScope.showload();
      $http.post(Cfg.rest, {action: 'login', user: $scope.login.user, pass: $scope.login.pass}).then(
				function (response, status, headers, config) {
  $rootScope.hideload();

  if (response.data.res == 'OK') {
    $rootScope.user_id = response.data.id;
    localStorage.setItem('HomeClassID', $rootScope.user_id);
    localStorage.setItem('HomeClassProfile', response.data.profile);

				      $http.post(Cfg.rest, {action: 'getNotifications',
				                            id: localStorage.getItem('HomeClassID'),
				                            perfil: localStorage.getItem('HomeClassProfile'),
				                           }).then(
				        function (response, status, headers, config) {
				          $rootScope.qtynotify = response.data.notifyUnread;
				          $rootScope.notify = response.data.notify;
				          $rootScope.minnotify = response.data.notify.length;
				        }
				      );


    if (response.data.profile == 'profesor') {

      if (window.cordova) {
        var push = PushNotification.init({
								  android: {
								    senderID: '874092342410'
								  },
								  ios: {
								    alert: 'true',
								    badge: true,
								    sound: 'false'
								  }
        });
        push.on('registration', function (data) {
								  $http.post(Cfg.rest, {
								        action: 'send_token',
								        token: data.registrationId,
								        tipo: 'profesor',
								        id: $rootScope.user_id,
								        os: cordova.platformId
								      }).then(
								  function (response, status, headers, config) {

								  },
								  function () {

								  });
        });
      }


      $state.go('main.profehome');
    }
    else {


      if (window.cordova) {
        var push = PushNotification.init({
								  android: {
								    senderID: '874092342410'
								  },
								  ios: {
								    alert: 'true',
								    badge: true,
								    sound: 'false'
								  }
        });
        push.on('registration', function (data) {
								  $http.post(Cfg.rest, {
								        action: 'send_token',
								        token: data.registrationId,
								        tipo: 'apoderado',
								        id: $rootScope.user_id,
								        os: cordova.platformId
								      }).then(
								  function (response, status, headers, config) {

								  },
								  function () {

								  });
        });
      }

      $state.go('main.search');
    }

  }
  else {
    $rootScope.err(response.data.msg);
  }
},
				function () {
  $rootScope.hideload();
  $rootScope.err();
});
    }
  };
});


'use strict';
angular
  .module('auth')
  .component('homeclassHeader', {
    templateUrl: 'auth/templates/components/homeclass-header.html',
    controllerAs: 'head'
  });

'use strict';
var App = angular.module('HomeClass', [
  'main',
  'auth',
  'angularTrix',
  'ngTagsInput',
  'ionic.rating',
  'textAngular'
]);
App.config(function ($provide) {
  $provide.decorator('taOptions', ['taRegisterTool', '$delegate', function (taRegisterTool, taOptions) {

    taOptions.toolbar = [
      ['quote', 'bold', 'italics', 'underline', 'strikeThrough', 'ul', 'ol', 'redo', 'undo']
    ];
    return taOptions;
  }]);
});

App.run(function ($rootScope, $ionicPlatform, $ionicHistory, $ionicPopup, $ionicSideMenuDelegate, $cordovaGeolocation, $state, $timeout, $http, $ionicLoading, Cfg) {
  $ionicPlatform.ready(function () {
    $ionicSideMenuDelegate.canDragContent(false);

    if (window.cordova && window.cordova.plugins.Keyboard) {
      window.cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      window.cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      //StatusBar.styleDefault();
      window.StatusBar.overlaysWebView(false);
      //StatusBar.styleLightContent();
    }
		/*setTimeout(function() {
		    if(navigator.splashscreen) {
		     navigator.splashscreen.hide();
		    }
		}, 100);*/
  });
    // Disable BACK button on home
  $ionicPlatform.registerBackButtonAction(function (event) {
    if ($ionicHistory.backView() !== null) {
      navigator.app.backHistory();
    }
    else {
      event.preventDefaul();
    }
  }, 100);
  $rootScope.qtynotify = 0;
  $rootScope.minnotify = 0;
  $rootScope.notify = [];

  if (localStorage.getItem('HomeClassID') !== null) {
    $http.post(Cfg.rest, {action: 'getNotifications',
      id: localStorage.getItem('HomeClassID'),
      perfil: localStorage.getItem('HomeClassProfile'),
    }).then(
      function (response, status, headers, config) {
        $rootScope.qtynotify = response.data.notifyUnread;
        $rootScope.notify = response.data.notify;
        $rootScope.minnotify = response.data.notify.length;
        $rootScope.conf = config;
      }
    );
  }

  setInterval(function () {
    if (localStorage.getItem('HomeClassID') !== null) {
      $http.post(Cfg.rest, {action: 'getNotifications',
        id: localStorage.getItem('HomeClassID'),
        perfil: localStorage.getItem('HomeClassProfile'),
      }).then(
          function (response, status, headers, config) {
            $rootScope.qtynotify = response.data.notifyUnread;
            $rootScope.notify = response.data.notify;
            $rootScope.minnotify = response.data.notify.length;
            $rootScope.conf = config;
          }
        );
    }
  }, 10000);
  $rootScope.comparte = function () {
    window.plugins.socialsharing.share('Conoce Homeclass, clases a 1 click', null, null, 'http://www.homeclass.cl');
  };

  $rootScope.contacto = function () {
    window.open('http://www.homeclass.cl/app/contacto.php', '_system');
  };

  $rootScope.err = function (msg) {
    var alertPopup = $ionicPopup.alert({
      title: 'Error',
      template: (msg ? msg : 'Error al consultar el servicio. Intente más tarde')
    });
    alertPopup.then(function () {});
  };

  $rootScope.ok = function (msg, callback) {
    var alertPopup = $ionicPopup.alert({
      title: 'Listo',
      template: (msg ? msg : 'Operación realizada con éxito')
    });

    alertPopup.then(function () {
      if (callback) { callback(); }
    });
  };

  $rootScope.showload = function (msg) {
    $ionicLoading.show({
      template: '<ion-spinner></ion-spinner>' + (msg ? '<br>' + msg : '')
    }).then(function () {});
  };
  $rootScope.confirmar = function (msg, callback, no) {
    var confirmPopup = $ionicPopup.confirm({
      title: 'Confirmar', //'Confirmar',
      template: (msg ? msg : '¿Desea continuar?'),
      buttons: [
        {
          text: 'No',
          type: 'button-calm',
          onTap: function () { if (no) { document.body.classList.remove('modal-open'); no(); } }
        },
        {
          text: '<b>Aceptar</b>',
          type: 'button-positive',
          onTap: function () {
            document.body.classList.remove('modal-open');
            callback();
          }
        },
      ]
    });

    confirmPopup.then(function () {});

  };
  $rootScope.hideload = function () {
    $ionicLoading.hide().then(function () { });
  };

  $rootScope.formatDate = function (date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2) {month = '0' + month;}
    if (day.length < 2) {day = '0' + day;}
    return [year, month, day].join('-');
  };
  $rootScope.formatFecha = function (date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2) {month = '0' + month;}
    if (day.length < 2) {day = '0' + day;}
    return [day, month, year].join('-');
  };

  $rootScope.convertToDate = function (stringDate) {
    var dateOut = new Date(stringDate);
    dateOut.setDate(dateOut.getDate() + 1);
    return dateOut;
  };

  $rootScope.cerrarSesion = function () {
    $rootScope.confirmar('¿Deseas cerrar sesión?', function () {
      $ionicHistory.clearCache();
      $ionicHistory.clearHistory();
      $ionicHistory.nextViewOptions({
        historyRoot: true
      });

      localStorage.removeItem('HomeClassID');
      localStorage.removeItem('HomeClassProfile');
      $rootScope.soyProfesor = false;
      $rootScope.soyApoderado = false;
      $rootScope.soyAlumno = false;
      $state.go('login');
    });

  };

  $rootScope.reportAppLaunched = function (url) {
    console.log('External Query: ' + url);
    var split = '';
    if (url.indexOf('rechazo') >= 0) {
      split = url.split('/');
      /*alert('a url '+url);
      alert('a split0 '+split[0]);
      alert('a split1 '+split[1]);*/
      $state.go('main.rechazo', { trx: split[1] } );
    }
    else if (url.indexOf('exito') >= 0) {
      split = url.split('/');
      /*alert('b url '+url);
      alert('b split0 '+split[0]);
      alert('b split1 '+split[1]);*/
      $state.go('main.exito', { trx: split[1] });
    }
  };

  var state = 'login';
  if ( localStorage.getItem('onBoarding') === null ) {
    console.log('ONBOARDING');
    state = 'welcome';
  } else if ( localStorage.getItem('HomeClassID') !== null ) {
    if (localStorage.getItem('HomeClassProfile') === 'profesor') {
      state = 'main.profehome';
    }
    else {
      state = 'main.search';
    }
  }
  $state.go(state);
});

App.factory('Cfg', function ($window) {
  console.log($window);
  return {
      //rest : 'http://localhost/homeclass/ws.php'
    rest: 'http://admin.homeclass.cl/ws.php',
  };
});

Array.prototype.filterObjects = function (key, value) {
  return this.filter(function (x) { return x[key] === value; });
};

/*
function handleOpenURL (url) {
  var body = document.getElementsByTagName('body')[0];
  var mainController = angular.element(body).scope().$root;
  setTimeout(function () {
    //alert(url);
    mainController.reportAppLaunched(url);
  }, 0);
}*/
