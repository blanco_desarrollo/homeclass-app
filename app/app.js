'use strict';
var App = angular.module('HomeClass', [
  'main',
  'auth',
  'angularTrix',
  'ngTagsInput',
  'ionic.rating',
  'textAngular'
]);
App.config(function ($provide) {
  $provide.decorator('taOptions', ['taRegisterTool', '$delegate', function (taRegisterTool, taOptions) {

    taOptions.toolbar = [
      ['quote', 'bold', 'italics', 'underline', 'strikeThrough', 'ul', 'ol', 'redo', 'undo']
    ];
    return taOptions;
  }]);
});

App.run(function ($rootScope, $ionicPlatform, $ionicHistory, $ionicPopup, $ionicSideMenuDelegate, $cordovaGeolocation, $state, $timeout, $http, $ionicLoading, Cfg) {
  $ionicPlatform.ready(function () {
    $ionicSideMenuDelegate.canDragContent(false);

    if (window.cordova && window.cordova.plugins.Keyboard) {
      window.cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      window.cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      //StatusBar.styleDefault();
      window.StatusBar.overlaysWebView(false);
      //StatusBar.styleLightContent();
    }
		/*setTimeout(function() {
		    if(navigator.splashscreen) {
		     navigator.splashscreen.hide();
		    }
		}, 100);*/
  });
    // Disable BACK button on home
  $ionicPlatform.registerBackButtonAction(function (event) {
    if ($ionicHistory.backView() !== null) {
      navigator.app.backHistory();
    }
    else {
      event.preventDefaul();
    }
  }, 100);
  $rootScope.qtynotify = 0;
  $rootScope.minnotify = 0;
  $rootScope.notify = [];

  if (localStorage.getItem('HomeClassID') !== null) {
    $http.post(Cfg.rest, {action: 'getNotifications',
      id: localStorage.getItem('HomeClassID'),
      perfil: localStorage.getItem('HomeClassProfile'),
    }).then(
      function (response, status, headers, config) {
        $rootScope.qtynotify = response.data.notifyUnread;
        $rootScope.notify = response.data.notify;
        $rootScope.minnotify = response.data.notify.length;
        $rootScope.conf = config;
      }
    );
  }

  setInterval(function () {
    if (localStorage.getItem('HomeClassID') !== null) {
      $http.post(Cfg.rest, {action: 'getNotifications',
        id: localStorage.getItem('HomeClassID'),
        perfil: localStorage.getItem('HomeClassProfile'),
      }).then(
          function (response, status, headers, config) {
            $rootScope.qtynotify = response.data.notifyUnread;
            $rootScope.notify = response.data.notify;
            $rootScope.minnotify = response.data.notify.length;
            $rootScope.conf = config;
          }
        );
    }
  }, 10000);
  $rootScope.comparte = function () {
    window.plugins.socialsharing.share('Conoce Homeclass, clases a 1 click', null, null, 'http://www.homeclass.cl');
  };

  $rootScope.contacto = function () {
    window.open('http://www.homeclass.cl/app/contacto.php', '_system');
  };

  $rootScope.err = function (msg) {
    var alertPopup = $ionicPopup.alert({
      title: 'Error',
      template: (msg ? msg : 'Error al consultar el servicio. Intente más tarde')
    });
    alertPopup.then(function () {});
  };

  $rootScope.ok = function (msg, callback) {
    var alertPopup = $ionicPopup.alert({
      title: 'Listo',
      template: (msg ? msg : 'Operación realizada con éxito')
    });

    alertPopup.then(function () {
      if (callback) { callback(); }
    });
  };

  $rootScope.showload = function (msg) {
    $ionicLoading.show({
      template: '<ion-spinner></ion-spinner>' + (msg ? '<br>' + msg : '')
    }).then(function () {});
  };
  $rootScope.confirmar = function (msg, callback, no) {
    var confirmPopup = $ionicPopup.confirm({
      title: 'Confirmar', //'Confirmar',
      template: (msg ? msg : '¿Desea continuar?'),
      buttons: [
        {
          text: 'No',
          type: 'button-calm',
          onTap: function () { if (no) { document.body.classList.remove('modal-open'); no(); } }
        },
        {
          text: '<b>Aceptar</b>',
          type: 'button-positive',
          onTap: function () {
            document.body.classList.remove('modal-open');
            callback();
          }
        },
      ]
    });

    confirmPopup.then(function () {});

  };
  $rootScope.hideload = function () {
    $ionicLoading.hide().then(function () { });
  };

  $rootScope.formatDate = function (date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2) {month = '0' + month;}
    if (day.length < 2) {day = '0' + day;}
    return [year, month, day].join('-');
  };
  $rootScope.formatFecha = function (date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2) {month = '0' + month;}
    if (day.length < 2) {day = '0' + day;}
    return [day, month, year].join('-');
  };

  $rootScope.convertToDate = function (stringDate) {
    var dateOut = new Date(stringDate);
    dateOut.setDate(dateOut.getDate() + 1);
    return dateOut;
  };

  $rootScope.cerrarSesion = function () {
    $rootScope.confirmar('¿Deseas cerrar sesión?', function () {
      $ionicHistory.clearCache();
      $ionicHistory.clearHistory();
      $ionicHistory.nextViewOptions({
        historyRoot: true
      });

      localStorage.removeItem('HomeClassID');
      localStorage.removeItem('HomeClassProfile');
      $rootScope.soyProfesor = false;
      $rootScope.soyApoderado = false;
      $rootScope.soyAlumno = false;
      $state.go('login');
    });

  };

  $rootScope.reportAppLaunched = function (url) {
    console.log('External Query: ' + url);
    var split = '';
    if (url.indexOf('rechazo') >= 0) {
      split = url.split('/');
      /*alert('a url '+url);
      alert('a split0 '+split[0]);
      alert('a split1 '+split[1]);*/
      $state.go('main.rechazo', { trx: split[1] } );
    }
    else if (url.indexOf('exito') >= 0) {
      split = url.split('/');
      /*alert('b url '+url);
      alert('b split0 '+split[0]);
      alert('b split1 '+split[1]);*/
      $state.go('main.exito', { trx: split[1] });
    }
  };

  var state = 'login';
  if ( localStorage.getItem('onBoarding') === null ) {
    console.log('ONBOARDING');
    state = 'welcome';
  } else if ( localStorage.getItem('HomeClassID') !== null ) {
    if (localStorage.getItem('HomeClassProfile') === 'profesor') {
      state = 'main.profehome';
    }
    else {
      state = 'main.search';
    }
  }
  $state.go(state);
});

App.factory('Cfg', function ($window) {
  console.log($window);
  return {
      //rest : 'http://localhost/homeclass/ws.php'
    rest: 'http://admin.homeclass.cl/ws.php',
  };
});

Array.prototype.filterObjects = function (key, value) {
  return this.filter(function (x) { return x[key] === value; });
};

/*
function handleOpenURL (url) {
  var body = document.getElementsByTagName('body')[0];
  var mainController = angular.element(body).scope().$root;
  setTimeout(function () {
    //alert(url);
    mainController.reportAppLaunched(url);
  }, 0);
}*/
