'use strict';
angular.module('main')
.controller('CreditosprofeCtrl', function ($scope, $ionicModal, $log, $filter, $http, $state, $stateParams, $ionicLoading, $rootScope, $ionicNavBarDelegate, Cfg) {

  $scope.creditos = 0;
  $rootScope.showload();
  $scope.wins = [];


  $http.post(Cfg.rest, {action: 'getGanancias', id: localStorage.getItem('HomeClassID')}).then(
  function (response, status, headers, config) {
    $scope.wins = response.data.wins;
    $rootScope.hideload();
    if ($scope.wins) {
      for (var i = 0; i < $scope.wins.length ; i++) {
        if ($scope.wins[i].clase_realizada == 1) { $scope.creditos++; }
         // $scope.wins[i].newdate = $rootScope.convertToDate($scope.wins[i].fecha_pago);
          //$scope.compras[i].newdate = $filter('date')(new Date($scope.compras[i].fecha_pago),'yyyy-MM-dd');
      }
    }

  });
});
