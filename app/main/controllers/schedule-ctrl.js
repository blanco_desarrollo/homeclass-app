'use strict';
angular.module('main')
.controller('ScheduleCtrl', function ($log, $scope) {

  var vm = this;
  vm.info = {};
  vm.slider = null;

  vm.options = {
    loop: false,
    effect: 'slide',
    speed: 500
  };

  vm.nextDate = nextDate;
  vm.previousDate = previousDate;

  function nextDate () {
    if (vm.slider !== null) {
      vm.slider.slideNext();
    }
  }

  function previousDate () {
    if (vm.slider !== null) {
      vm.slider.slidePrev();
    }
  }

  $scope.$on('$ionicSlides.sliderInitialized', function (event, data) {
    vm.slider = data.slider;
  });

});
