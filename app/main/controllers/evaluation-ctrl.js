'use strict';
angular.module('main')
.controller('EvaluationCtrl', function ($scope, $ionicModal, $log, $http, $ionicPopup, $state, $stateParams, $ionicLoading, $rootScope, $ionicNavBarDelegate, Cfg) {

  $scope.evaluation = {id: ''};
  $scope.profesores = [];

  $scope.startMe = function () {
    $rootScope.showload();
    $http.post(Cfg.rest, {action: 'clasesPasadoEvaluar', id: localStorage.getItem('HomeClassID')}).then(
    function (response, status, headers, config) {
      $scope.profesores = response.data.profesores;
      $rootScope.hideload();
    },
    function () {
      $rootScope.hideload();
      $rootScope.err();
    });
  };

  $scope.startMe();

  $scope.evaluar = function () {
    if (!vm.hasOwnProperty('question1')) { $rootScope.err('Responda la pregunta 1 por favor'); }
    else if (!vm.hasOwnProperty('question2')) { $rootScope.err('Responda la pregunta 2 por favor'); }
    else if (!vm.hasOwnProperty('question3')) { $rootScope.err('Responda la pregunta 3 por favor'); }
    else {
      $rootScope.confirmar('¿Desea enviar la evaluación? Podrá re-evaluar en próxima clase', function () {
        $rootScope.showload();
        $http.post(Cfg.rest, {action: 'evaluarProfesor',
          evaluador: localStorage.getItem('HomeClassID'),
          clase: $scope.evaluation.id,
          preg1: (vm.question1.yes ? '1' : ''),
          preg2: (vm.question2.yes ? '1' : ''),
          preg3: (vm.question3.yes ? '1' : ''),
        }
                  ).then(
        function (response, status, headers, config) {
          $scope.evaluation.id = '';
          $rootScope.hideload();
          $scope.ok('Gracias por evaluar al profesor', function () {
            $scope.startMe();
          });

        },
        function () {
          $rootScope.hideload();
          $rootScope.err();
        });
      });
    }
  };

  var vm = this;
  vm.yes = yes;
  vm.no = no;

  $scope.evaluable = false;

  function yes (question) {
    _toggle(question, true);
  }

  function no (question) {
    _toggle(question, false);
  }

  function _toggle (question, value) {
    var yes = false;
    var no = false;
    if (value === true) {
      yes = true;
    } else {
      no = true;
    }

    vm[question] = {
      'yes': yes,
      'no': no
    };

  }

});
