'use strict';
angular.module('main')
.controller('CreditsCtrl', function ($scope, $ionicModal, $log, $filter, $http, $state, $stateParams, $ionicLoading, $rootScope, $ionicNavBarDelegate, Cfg) {

  $scope.creditos = 0;
  $rootScope.showload();
  $scope.compras = [];


  $http.post(Cfg.rest, {action: 'getCompras', id: localStorage.getItem('HomeClassID')}).then(
  function (response, status, headers, config) {
    $scope.compras = response.data.compras;

    if ($scope.compras) {
      for (var i = 0; i < $scope.compras.length ; i++) {
        $scope.compras[i].newdate = $rootScope.convertToDate($scope.compras[i].fecha_pago);
          //$scope.compras[i].newdate = $filter('date')(new Date($scope.compras[i].fecha_pago),'yyyy-MM-dd');
      }
    }
    $http.post(Cfg.rest, {action: 'getMe', id: localStorage.getItem('HomeClassID'), profile: localStorage.getItem('HomeClassProfile')}).then(
    function (response, status, headers, config) {
      $scope.creditos = response.data.me.creditos;

      $rootScope.hideload();

    });

  });

  $scope.addcreditos = function () {
    $state.go('main.request', {add: '1'});
  };
});
