'use strict';
angular.module('main')
.controller('DashboardCtrl', function () {

  var vm = this;

  // User dashboard
  vm.dashboard = [
    {
      title: 'Busca tu clase',
      icon: 'main/assets/images/dash_search.svg',
      background: 'main/assets/images/block_background1.png',
      color: '#C5DF58'
    }, {
      title: 'Calendario',
      icon: 'main/assets/images/dash_calendar.svg',
      background: 'main/assets/images/block_background2.png',
      color: '#B2D918'
    }, {
      title: 'Historial clases',
      icon: 'main/assets/images/dash_history.svg',
      background: 'main/assets/images/block_background3.png',
      color: '#9FC604'
    }
  ];

  // Tutor dashboard
  vm.dashboard = [
    {
      title: 'Clases disponibles',
      icon: 'main/assets/images/dash_search.svg',
      background: 'main/assets/images/block_background4.png',
      color: '#FBA23B'
    }, {
      title: 'Calendario',
      icon: 'main/assets/images/dash_calendar.svg',
      background: 'main/assets/images/block_background2.png',
      color: '#FF9120'
    }, {
      title: 'Historial clases',
      icon: 'main/assets/images/dash_history.svg',
      background: 'main/assets/images/block_background3.png',
      color: '#F97E02'
    }
  ];

  // User footer
  vm.footer = {
    title: 'Comparte homeclass con tus amigos'
  };

  // Tutor footer
  vm.footer = {
    title: 'Contactanos'
  };

});
