'use strict';
angular.module('main')
  .controller('HistoryTutorCtrl', function ($scope, $ionicModal, $log, $http, $state, $stateParams, $ionicLoading, $rootScope, $ionicNavBarDelegate, Cfg) {
    var vm = this;
    $scope.class = [];
    $rootScope.showload();
    $http.post(Cfg.rest, {action: 'getProximasClases', id: localStorage.getItem('HomeClassID')}).then(
    function (response, status, headers, config) {
      $scope.class = response.data.class;
      if (response.data.class) { $scope.clases = response.data.class.length; }

      $rootScope.hideload();

    },
    function () {
      $rootScope.hideload();
      $rootScope.err();
    });

    vm.maxRating = 5;

    vm.toggleGroup = function (id) {
      if (vm.isGroupShown(id)) {
        vm.shownGroup = null;
      } else {
        vm.shownGroup = id;
      }
    };
    vm.isGroupShown = function (id) {
      return vm.shownGroup === id;
    };


  });
