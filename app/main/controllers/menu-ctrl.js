'use strict';
angular.module('main')
.controller('MenuCtrl', function ($http, $ionicModal, $rootScope, $state, $scope, Cfg) {
	$rootScope.soyAlumno = false;
	$rootScope.soyProfesor = false;
	if (localStorage.getItem("HomeClassProfile") == "profesor") {
		$rootScope.soyProfesor = true;
	}
	else {
		$rootScope.soyApoderado = true;
		$rootScope.soyAlumno = true;
	}
	$rootScope.avatar = "main/assets/images/menu/avatar.png";

	$http.post(Cfg.rest, {action: 'getMe', id: localStorage.getItem("HomeClassID"), profile: localStorage.getItem("HomeClassProfile")}).then(
	  function(response, status, headers, config) {
	    $rootScope.me = response.data.me;
	    if (response.data.me && response.data.me.hasOwnProperty("avatar")) {
	      console.log(response.data.me);
	    	$rootScope.avatar = response.data.me.avatar;
	    }
	    if (response.data.me.hasOwnProperty('file_foto')) {
	    	$rootScope.avatar = response.data.me.file_foto;
	    }

  	});

  $scope.closeOpenedNotify = function () {
    $scope.modalNotify.hide();
    setTimeout(function () { $scope.modalNotify.remove(); }, 800);
  };

  $scope.openedNotify = {};
  $scope.modalNotify = null;
  	$scope.openNotify = function (not) {
  		$scope.openedNotify = not;
    $ionicModal.fromTemplateUrl('main/templates/notify.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.modalNotify = modal;
      $scope.modalNotify.show();
      $http.post(Cfg.rest, {action: 'notifyReader', id: not.id});
    });
  	};

  	$scope.notifyOk = function (notid) {
  		$rootScope.showload();
  		$http.post(Cfg.rest, {action: 'notifyOk', id: notid}).then(function () {
	      $http.post(Cfg.rest, {action: 'getNotifications',
	                            id: localStorage.getItem('HomeClassID'),
	                            perfil: localStorage.getItem('HomeClassProfile'),
	                           }).then(
	        function (response, status, headers, config) {
	          $rootScope.qtynotify = response.data.notifyUnread;
	          $rootScope.notify = response.data.notify;
	          $rootScope.minnotify = response.data.notify.length;
	          $rootScope.hideload();
			  $scope.modalNotify.hide();
			  setTimeout(function () { $scope.modalNotify.remove(); }, 300);

	        }
	      );
	    });
  	};

});
