'use strict';
angular.module('main')
.controller('ProfehomeCtrl', function ($scope, $log, $http, $state, $ionicLoading, $rootScope, Cfg) {

  $scope.comunas = '';
  $scope.horarios = '';
  $scope.creditos = '';

  $rootScope.showload();

  $http.post(Cfg.rest, {action: 'getProfesorDashboard', id: localStorage.getItem('HomeClassID')}).then(
    function (response, status, headers, config) {

      $scope.comunas = response.data.comunas;
      $scope.horarios = response.data.horarios;
      $scope.creditos = response.data.creditos;

      $rootScope.hideload();
    },
    function () {
      $rootScope.hideload();
      $rootScope.err();
    });


  $scope.gotoMe = function (d) {
    $state.go(d);
  };
});
///
