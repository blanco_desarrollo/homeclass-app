'use strict';
angular.module('main').controller('SearchCtrl', function ($scope, $ionicModal, $log, $http, $state, $ionicLoading, $rootScope, $cordovaGeolocation, Cfg) {

  $rootScope.comunas = [];
  $rootScope.horarios = [];
  $rootScope.materias = [];
  $scope.total_profes = 0;
  var hoy = new Date();
  $scope.search = { comuna: null, materia: null, horario: null, fecha: null, dia: 0, horarioid: 0 };
  $rootScope.busqueda = { resultados: [], comuna: '', materia: '', horario: '', fecha: null, dia: 0, total: 0, horarioid: 0 };

  $scope.loadMaster = function () {
    $rootScope.showload();
    $http.post(Cfg.rest, {action: 'master'}).then(
        function (response, status, headers, config) {
          $rootScope.hideload();
          $rootScope.materias = response.data.materia;
          $rootScope.comunas = response.data.comuna;
          $rootScope.horarios = response.data.horario;

          /* Posicion de GPS + Precios de mi comuna */
          var posOptions = {timeout: 5000, enableHighAccuracy: false};
          $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
            var uri = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' + position.coords.latitude + ',' + position.coords.longitude + '&sensor=false';
            $rootScope.la_comuna = '';
            $http.get(uri).then(function (res, status, headers, config) {
                //$rootScope.miDireccion = res.data.results[0].formatted_address;
                //$rootScope.miComuna = res.data.results[0].address_components[2].long_name;

              res.data.results[0].address_components.forEach(function (item) {
                for (var q = 0; q < item.types.length ; q++) {
                  if (item.types[q] == 'administrative_area_level_1') {
                      // region
                  }
                  if (item.types[q] == 'administrative_area_level_2') {
                    $rootScope.la_provincia = item.long_name;
                  }
                  if (item.types[q] == 'locality' || item.types[q] == 'administrative_area_level_3') {
                    $rootScope.la_comuna = item.long_name;
                  }
                }
              });

              if ($rootScope.la_comuna == '') {
                $rootScope.la_comuna = $rootScope.la_provincia;
              }

                // buscar comuna en maestra
              var found = 0;
              for (var jk = 0; jk < $rootScope.comunas.length; jk++) {
                if ($rootScope.comunas[jk].com_nombre == $rootScope.la_comuna.toUpperCase()) {
                  var found = 1;
                  $scope.search.comuna = $rootScope.comunas[jk].com_id;
                }
              }

              if (found == 0) {
                $rootScope.err('No hemos podido encontrar profesores en ' + $rootScope.la_comuna + ', por favor selecciona la comuna más cercana');
              }

            }, function () {
              $rootScope.err('No hemos podido cargar tu comuna vía GPS, por favor selecciónala de la lista');
            });
          });


        },
        function () {
          $rootScope.hideload();
          $rootScope.err();
        });
  };

  $scope.loadMaster();

    /*
    $ionicModal.fromTemplateUrl('main/templates/modal/soon.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.modal = modal;
      //$scope.modal.show();
    });
  */

  $scope.updateTotal = function () {
      //console.log($rootScope.comunas.filterObjects("com_id", $scope.search.comuna)); // tambien OK
    $scope.total_profes = $rootScope.comunas.filter(function (a) { return a.com_id == $scope.search.comuna; })[0].total;
  };

  $scope.buscar = function () {
    var hoy = new Date();
    var dif = ( $scope.search.fecha != null ? ((hoy.getTime() - $scope.search.fecha.getTime()) / 1000) : 0);
    if (
          ( $scope.search.comuna == null || $scope.search.comuna == '' ) &&
          ( $scope.search.materia == null || $scope.search.materia == '' ) &&
          ( $scope.search.horario == null || $scope.search.horario == '' )
         ) {
      $rootScope.err('Por favor defina a lo menos 1 parametro de búsqueda');
    }
    else if (dif >= 86400) {
      $rootScope.err('Fecha no puede ser del pasado');
    }
    else {
      $rootScope.showload();

      $http.post(Cfg.rest, {
        action: 'buscar',
        comuna: $scope.search.comuna,
        materia: $scope.search.materia,
        horario: $scope.search.horario,
        fecha: ($scope.search.fecha != null ? $rootScope.formatDate($scope.search.fecha) : '')
      }).then(
          function (response, status, headers, config) {
            $rootScope.hideload();
            $rootScope.busqueda.resultados = response.data.total;

            if ($scope.search.comuna != '' && $scope.search.comuna != null) {
              $rootScope.busqueda.comuna = $rootScope.comunas.filter(function (a) { return a.com_id == $scope.search.comuna; })[0].com_nombre;
            }
            if ($scope.search.materia != '' && $scope.search.materia != null) {
              $rootScope.busqueda.materia = $rootScope.materias.filter(function (a) { return a.id == $scope.search.materia; })[0].valor;
            }
            if ($scope.search.horario != '' && $scope.search.horario != null) {
              $rootScope.busqueda.horarioid = $scope.search.horario;
              $rootScope.busqueda.horario = '' + $rootScope.horarios.filter(function (a) { return a.id == $scope.search.horario; })[0].inicio + ' a ' + $rootScope.horarios.filter(function (a) { return a.id == $scope.search.horario; })[0].fin;
            }
            $rootScope.busqueda.fecha = $scope.search.fecha;
            $rootScope.busqueda.dia = $scope.search.dia;

            if (response.data.total > 0) {
              $rootScope.busqueda.resultados = response.data.rows;
            }
            else {
              $rootScope.busqueda.resultados = [];
            }

            $state.go('main.results');
          },
          function () {
            $rootScope.hideload();
            $rootScope.err();
          });
    }
  };

  var vm = this;

  function nextMonth () {
    vm.calendarDate.setMonth(vm.calendarDate.getMonth() + 1);
    _refreshCalendar();
  }

  function previousMonth () {
    vm.calendarDate.setMonth(vm.calendarDate.getMonth() - 1);
    _refreshCalendar();
  }

  function _refreshCalendar (scheduledDays) {
    if (scheduledDays === undefined) {
      scheduledDays = [];
    }
    vm.calendarArray = _getMonthArray(vm.calendarDate.getMonth(), vm.calendarDate.getFullYear(), scheduledDays);
  }

  function _getMonthArray (month, year, scheduledDays) {
    var finalArray = [];

    if (scheduledDays === undefined) {
      scheduledDays = [];
    }

    var today = new Date();

    var firstDayDate = new Date(year, month, 1);
    var lastDayPreviousMonth = new Date(year, month, 0).getDate();
    var lastDay = new Date(year, month + 1, 0).getDate();

    var len = lastDay + firstDayDate.getDay() - 1;

    var rows = Math.ceil(len / 7);

    for (var i = 0; i < rows; i++) {
      finalArray[i] = [];
    }

    var startFrom = lastDayPreviousMonth - (firstDayDate.getDay() - 1);

    for (i = 1; i < firstDayDate.getDay(); i++) {
      finalArray[0][i - 1] = {
        day: startFrom + i,
        class: 'previous'
      };
    }

    var cssClass = 'current';

    var dayCount = 1;
    var nextMonthCount = false;
    for (var r = 0; r < rows; r++) {
      for (var c = 0; c < 7; c++) {
        if (dayCount > lastDay) {
          dayCount = 1;
          nextMonthCount = true;
          cssClass = 'next';
        }

        if (finalArray[r][c] === undefined) {
          finalArray[r][c] = {
            day: dayCount,
            class: cssClass
          };

          if (scheduledDays.indexOf(dayCount) > -1 && nextMonthCount === false) {
            finalArray[r][c].class = 'today';
          }
          dayCount++;
        }
      }
    }

    return finalArray;
  }

  vm.calendarDate = new Date();
  var sc = []; //[vm.calendarDate.getDate()];
  vm.calendarArray = _getMonthArray(vm.calendarDate.getMonth(), vm.calendarDate.getFullYear(), sc);
  vm.nextMonth = nextMonth;
  vm.previousMonth = previousMonth;

  $scope.loadDia = function (dia, o, xclass) {
    if (xclass != 'previous' && xclass != 'next') {
      var sc = [];
      sc[0] = dia;
      vm.calendarArray = _getMonthArray(vm.calendarDate.getMonth(), vm.calendarDate.getFullYear(), sc);
      var nd = new Date(vm.calendarDate.getFullYear(), vm.calendarDate.getMonth(), dia);
      $scope.search.fecha = nd;
      $scope.search.dia = dia;
    }
  };
  //$scope.loadDia(hoy.getDate(), hoy);

});
