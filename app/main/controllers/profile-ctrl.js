'use strict';
angular.module('main')
.controller('ProfileCtrl', function ($scope, $ionicModal, $log, $http, $ionicPopup, $state, $stateParams, $cordovaCamera, $ionicLoading, $rootScope, $ionicNavBarDelegate, Cfg) {
	//$scope.avatar = "main/assets/images/foto-perfil.png";
  $scope.cover = '../../main/assets/images/cover-photo.png';
  $scope.yo = { nombre: '', email: '', childs: [], pass: '', nuevos: [{id: 0, alumno: '', deletable: 0}], bio: '', universidad: 0, intereses: [], direccion: '', especialidad: ''};
  $scope.activeChild = 0;
  $scope.editor = null;
  $rootScope.showload();

  $scope.getBackgroundStyle = function (imagepath) {
    return {
      'background-image': 'url(' + imagepath + ')'
    };
  };

  $http.post(Cfg.rest, {action: 'getMe', id: localStorage.getItem('HomeClassID'), profile: localStorage.getItem('HomeClassProfile')}).then(
	  function (response, status, headers, config) {

	    $scope.yo.nombre = response.data.me.nombre;
	    $scope.yo.email = response.data.me.email;
	    $scope.yo.pass = response.data.me.pass;

    if (response.data.me.hasOwnProperty('avatar')) {
      $scope.avatar = response.data.me.avatar;
    }
    if (response.data.me.hasOwnProperty('file_foto')) {
        // discrimina profesores
      $scope.avatar = response.data.me.file_foto;
      $scope.yo.universidad = response.data.me.universidad_id;
      $scope.yo.especialidad = response.data.me.especialidad;
      $scope.yo.bio = response.data.me.bio;
      var sp = response.data.me.intereses.split(',');
      var o = [];
      for (var a = 0;a < sp.length;a++) {
        o.push({text: sp[a]});
      }
      $scope.yo.intereses = o;
        //$scope.editor.setSelectedRange([0, 0])
        //$scope.editor.insertHTML(response.data.me.bio)
    }
    else {
        // alumno
      $scope.yo.direccion = response.data.me.direccion;
    }
	    $scope.yo.childs = response.data.me.childs;

    $http.post(Cfg.rest, {action: 'master'}).then(
        function (response, status, headers, config) {
          $rootScope.hideload();
          $scope.universidades = response.data.universidades;
        },
        function () {
          $rootScope.hideload();
          $rootScope.err();
        });

	  },
	  function () {
	    $rootScope.hideload();
	    $rootScope.err();
  });

  $scope.setImage = function () {
    $ionicPopup.show({
      template: '',
      title: 'Actualizar imagen',
      subTitle: 'Seleccione origen de imagen',
      scope: $scope,
      buttons: [
        {
          text: '<b>Cámara</b>',
          type: 'button-positive',
          onTap: function (e) {
            $scope.takePicture();
          }
        },
        {
          text: '<b>Galería</b>',
          type: 'button-positive',
          onTap: function (e) {
            $scope.selectPicture();
          }
        },
        {
          text: '<b>Cerrar</b>',
          type: 'button-calm',
          onTap: function (e) {
            return false;
          }
        }

      ]
    });
  };

  $scope.deleteAlumno = function (index) {
    $scope.yo.childs.splice(index, 1);
  };

  $scope.deleteNuevo = function (index) {
    $scope.yo.nuevos.splice(index, 1);
  };


  $scope.addNuevo = function (index) {
    console.log(index);
    if ($scope.yo.nuevos[index].alumno == '') {
      $rootScope.err('Debe ingresar nombre alumno');
    }
    else {
      $scope.yo.nuevos[index].deletable = 1;
      var o = { id: 0, nombre: '', deletable: 0};
      $scope.yo.nuevos.push(o);
    }
  };

  $scope.saveProfile = function () {
    $scope.showload();
    // timeout por tags waiting
    setTimeout(function () {
      var output = [];
      for (var u = 0;u < $scope.yo.intereses.length;u++) {
        output.push($scope.yo.intereses[u].text);
      }
      if (output.length > 0) {
        var interesesString = output.join(',');
      } else {
        var interesesString = '';
      }

      var postData = {
        action: 'updateProfile',
        id: localStorage.getItem('HomeClassID'),
        profile: localStorage.getItem('HomeClassProfile'),
        firstName: $scope.yo.nombre,
        email: $scope.yo.email,
        password: $scope.yo.pass,
        avatar: ($scope.avatar != 'main/assets/images/foto-perfil.png' ? $scope.avatar : ''),
        bio: $scope.yo.bio,
        intereses: interesesString,
        direccion: $scope.yo.direccion,
        universidad: $scope.yo.universidad,
        especialidad: $scope.yo.especialidad
      };

      $rootScope.avatar = ($scope.avatar != 'main/assets/images/foto-perfil.png' ? $scope.avatar : 'main/assets/images/foto-perfil.png');

      $http.post(Cfg.rest, postData)
      .then(function (data, status, headers, config) {
        $scope.hideload();
        $rootScope.ok('Perfil actualizado con éxito');
      });
    }, 1000);
  };

  $scope.selectPicture = function () {
    if (window.cordova) {
      var options = {
        quality: 50,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
        targetWidth: 400,
        targetHeight: 400
      };

      $cordovaCamera.getPicture(options).then(
      function (imageURI) {
        $scope.uploadPicture(imageURI);
      },
      function (err) {
        $ionicLoading.show({template: 'Error al acceder a tu galería', duration: 1500});
      });
    }
    else {
      $scope.uploadPicture('R0lGODlhPQBEAPeoAJosM//AwO/AwHVYZ/z595kzAP/s7P+goOXMv8+fhw/v739/f+8PD98fH/8mJl+fn/9ZWb8/PzWlwv///6wWGbImAPgTEMImIN9gUFCEm/gDALULDN8PAD6atYdCTX9gUNKlj8wZAKUsAOzZz+UMAOsJAP/Z2ccMDA8PD/95eX5NWvsJCOVNQPtfX/8zM8+QePLl38MGBr8JCP+zs9myn/8GBqwpAP/GxgwJCPny78lzYLgjAJ8vAP9fX/+MjMUcAN8zM/9wcM8ZGcATEL+QePdZWf/29uc/P9cmJu9MTDImIN+/r7+/vz8/P8VNQGNugV8AAF9fX8swMNgTAFlDOICAgPNSUnNWSMQ5MBAQEJE3QPIGAM9AQMqGcG9vb6MhJsEdGM8vLx8fH98AANIWAMuQeL8fABkTEPPQ0OM5OSYdGFl5jo+Pj/+pqcsTE78wMFNGQLYmID4dGPvd3UBAQJmTkP+8vH9QUK+vr8ZWSHpzcJMmILdwcLOGcHRQUHxwcK9PT9DQ0O/v70w5MLypoG8wKOuwsP/g4P/Q0IcwKEswKMl8aJ9fX2xjdOtGRs/Pz+Dg4GImIP8gIH0sKEAwKKmTiKZ8aB/f39Wsl+LFt8dgUE9PT5x5aHBwcP+AgP+WltdgYMyZfyywz78AAAAAAAD///8AAP9mZv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAKgALAAAAAA9AEQAAAj/AFEJHEiwoMGDCBMqXMiwocAbBww4nEhxoYkUpzJGrMixogkfGUNqlNixJEIDB0SqHGmyJSojM1bKZOmyop0gM3Oe2liTISKMOoPy7GnwY9CjIYcSRYm0aVKSLmE6nfq05QycVLPuhDrxBlCtYJUqNAq2bNWEBj6ZXRuyxZyDRtqwnXvkhACDV+euTeJm1Ki7A73qNWtFiF+/gA95Gly2CJLDhwEHMOUAAuOpLYDEgBxZ4GRTlC1fDnpkM+fOqD6DDj1aZpITp0dtGCDhr+fVuCu3zlg49ijaokTZTo27uG7Gjn2P+hI8+PDPERoUB318bWbfAJ5sUNFcuGRTYUqV/3ogfXp1rWlMc6awJjiAAd2fm4ogXjz56aypOoIde4OE5u/F9x199dlXnnGiHZWEYbGpsAEA3QXYnHwEFliKAgswgJ8LPeiUXGwedCAKABACCN+EA1pYIIYaFlcDhytd51sGAJbo3onOpajiihlO92KHGaUXGwWjUBChjSPiWJuOO/LYIm4v1tXfE6J4gCSJEZ7YgRYUNrkji9P55sF/ogxw5ZkSqIDaZBV6aSGYq/lGZplndkckZ98xoICbTcIJGQAZcNmdmUc210hs35nCyJ58fgmIKX5RQGOZowxaZwYA+JaoKQwswGijBV4C6SiTUmpphMspJx9unX4KaimjDv9aaXOEBteBqmuuxgEHoLX6Kqx+yXqqBANsgCtit4FWQAEkrNbpq7HSOmtwag5w57GrmlJBASEU18ADjUYb3ADTinIttsgSB1oJFfA63bduimuqKB1keqwUhoCSK374wbujvOSu4QG6UvxBRydcpKsav++Ca6G8A6Pr1x2kVMyHwsVxUALDq/krnrhPSOzXG1lUTIoffqGR7Goi2MAxbv6O2kEG56I7CSlRsEFKFVyovDJoIRTg7sugNRDGqCJzJgcKE0ywc0ELm6KBCCJo8DIPFeCWNGcyqNFE06ToAfV0HBRgxsvLThHn1oddQMrXj5DyAQgjEHSAJMWZwS3HPxT/QMbabI/iBCliMLEJKX2EEkomBAUCxRi42VDADxyTYDVogV+wSChqmKxEKCDAYFDFj4OmwbY7bDGdBhtrnTQYOigeChUmc1K3QTnAUfEgGFgAWt88hKA6aCRIXhxnQ1yg3BCayK44EWdkUQcBByEQChFXfCB776aQsG0BIlQgQgE8qO26X1h8cEUep8ngRBnOy74E9QgRgEAC8SvOfQkh7FDBDmS43PmGoIiKUUEGkMEC/PJHgxw0xH74yx/3XnaYRJgMB8obxQW6kL9QYEJ0FIFgByfIL7/IQAlvQwEpnAC7DtLNJCKUoO/w45c44GwCXiAFB/OXAATQryUxdN4LfFiwgjCNYg+kYMIEFkCKDs6PKAIJouyGWMS1FSKJOMRB/BoIxYJIUXFUxNwoIkEKPAgCBZSQHQ1A2EWDfDEUVLyADj5AChSIQW6gu10bE/JG2VnCZGfo4R4d0sdQoBAHhPjhIB94v/wRoRKQWGRHgrhGSQJxCS+0pCZbEhAAOw==');
    }
  };


  $scope.takePicture = function () {
    var options = {
      quality: 50,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.CAMERA,
      targetWidth: 400,
      targetHeight: 400
    };
    $cordovaCamera.getPicture(options).then(
    function (imageData) {
      $scope.uploadPicture(imageData);
    },
    function (err) {
      $ionicLoading.show({template: 'Error al acceder a tu cámara', duration: 1500});
    });
  };

  $scope.uploadPicture = function (base64) {
    $ionicLoading.show({template: '<ion-spinner></ion-spinner>'});
    var base64Image = 'data:image/jpeg;base64,' + base64;
    var blob = dataURItoBlob(base64Image);
    var objURL = window.URL.createObjectURL(blob);
    var image = new Image();
    image.src = objURL;
    window.URL.revokeObjectURL(objURL);

    var formData = new FormData();
    formData.append('file', blob, 'avataruser.jpg');
    formData.append('upload_preset', 'homeclass');

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4 && xhr.status === 200) {
        var json = JSON.parse(xhr.responseText);
        $scope.avatar = json.url;
        $scope.cover = json.url;
        setTimeout(function () {
          $ionicLoading.hide();
        }, 2000);
      }
    };
    xhr.open('post', 'https://api.cloudinary.com/v1_1/dujuytngk/image/upload');
    xhr.send(formData);
  };

  function dataURItoBlob (dataURI) {

    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
      {byteString = atob(dataURI.split(',')[1]);}
    else
          {byteString = unescape(dataURI.split(',')[1]);}

    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ia], {type: mimeString});
  }

});
