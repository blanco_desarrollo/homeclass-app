'use strict';
angular.module('main')
.controller('ControlCtrl', function ($scope, $ionicModal, $log, $http, $ionicPopup, $state, $stateParams, $ionicLoading, $rootScope, $ionicNavBarDelegate, Cfg) {

  $scope.control = {profesor: '0', alumno: '0'};
  $scope.profesores = [];
  $scope.alumnos = [];
  $scope.porcentaje = 0;

  $rootScope.showload();

  $scope.buscarAlumno = function () {
  	  $rootScope.showload();
	  $http.post(Cfg.rest, {action: 'alumno_x_profesor', profesor: $scope.control.profesor, id: localStorage.getItem('HomeClassID') }).then(
	  function (response, status, headers, config) {
	    $scope.alumnos = response.data.alumnos;
	    $rootScope.hideload();
	  },
	  function () {
	    $rootScope.hideload();
	    $rootScope.err();
	  });

  };

  $scope.filtrarAlumno = function () {
  	  $scope.porc = false;

  	  if ($scope.control.alumno != '0') {

	  	  $rootScope.showload();
		  $http.post(Cfg.rest, {action: 'porcentaje_avance', profesor: $scope.control.profesor, alumno: $scope.control.alumno, id: localStorage.getItem('HomeClassID')}).then(
		  function (response, status, headers, config) {
    $scope.porcentaje = response.data.porcentaje;
		  	$scope.porc = true;
		    $rootScope.hideload();
		  },
		  function () {
		    $rootScope.hideload();
		    $rootScope.err();
		  });

  	  }


  };

  $http.post(Cfg.rest, {action: 'profesoresPasado', id: localStorage.getItem('HomeClassID')}).then(
  function (response, status, headers, config) {
    $scope.profesores = response.data.profesores;
    $rootScope.hideload();
  },
  function () {
    $rootScope.hideload();
    $rootScope.err();
  });

  $scope.evaluar = function () {
    $rootScope.confirmar('¿Desea enviar la evaluación? Podrá re-evaluar más adelante', function () {
      $rootScope.showload();
      setTimeout(function () {
        $scope.evaluation.id = 0;
        $rootScope.hideload();
        //$scope.ok("Gracias por evaluar al profesor");
      }, 2300);
    });
  };

  $scope.porc = false;

  var vm = this;
  vm.search = {};


  vm.maxProgress = 100;
  vm.studentProgress = 75;

});


