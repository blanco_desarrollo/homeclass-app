'use strict';
angular.module('main')
.controller('ChangeCtrl', function ($scope, $ionicModal, $log, $http, $ionicPopup, $state, $stateParams, $ionicLoading, $rootScope, $ionicNavBarDelegate, Cfg) {

  $scope.cambio = {str: ''};
  $scope.agendas = [];

  $rootScope.showload();

  $http.post(Cfg.rest, {action: 'profesoresFuturo', id: localStorage.getItem('HomeClassID')}).then(
  function (response, status, headers, config) {

    $scope.agendas = response.data.agendas;
    $rootScope.hideload();
  },
  function () {
    $rootScope.hideload();
    $rootScope.err();
  });

  $scope.cambio = function () {
    if ($scope.cambio.str != '') {
      $rootScope.confirmar('Se va a anular la clase y comenzara un nuevo proceso de busqueda, ¿desea continuar?', function () {

        $rootScope.showload();
        $http.post(Cfg.rest, {action: 'anularClase', id: $scope.cambio.str, autor: localStorage.getItem('HomeClassID')}).then(
        function (response, status, headers, config) {

          //$rootScope.preloadBuscar = { resultados: [], comuna: '', materia: '', horario: '', fecha: '', dia: 0, total: 0, horarioid: 0 };
          $state.go('main.search');

          $rootScope.hideload();
        },
        function () {
          $rootScope.hideload();
          $rootScope.err();
        });

      });
    }
  };

  var vm = this;

  vm.maxRating = 5;

  vm.teacher = {
    id: 1,
    name: 'Juan Perez',
    avatar: 'main/assets/images/person1.png',
    lesson: 'Historia y Geografía',
    university: 'U. de Chile',
    rating: 5,
    ratingQuantity: 20
  };

});
