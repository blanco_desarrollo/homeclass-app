'use strict';
angular.module('main')
.controller('RequestCtrl', function ($scope, $ionicModal, $log, $ionicHistory, $http, $state, $stateParams, $ionicLoading, $rootScope, $ionicNavBarDelegate, Cfg) {
  var vm = this;

  $rootScope.cargada = 0;
  $rootScope.tengo_puntos = 0;
  $scope.stParamAdd = $stateParams.add;

  if ($stateParams.add == '1') {

    $rootScope.showload();
    $http.post(Cfg.rest, {action: 'planes', id: localStorage.getItem('HomeClassID')}).then(
      function (response, status, headers, config) {
        $scope.planes = response.data.planes;
        $rootScope.cargada = 1;
        $rootScope.hideload();
      },
      function () {
        $rootScope.hideload();
        $rootScope.err();
      });

  }
  else {

    $rootScope.showload();
    $http.post(Cfg.rest, {action: 'planes', id: localStorage.getItem('HomeClassID')}).then(
      function (response, status, headers, config) {
        $scope.planes = response.data.planes;

        if (response.data.tengo_puntos > 0) {
          $rootScope.tengo_puntos = 1;
          $rootScope.confirmar('Tienes ' + response.data.tengo_puntos + ' creditos disponibles, ¿deseas agendar la clase ahora?', function () {

            $http.post(Cfg.rest, {
              action: 'ok_clase',
              user_id: localStorage.getItem('HomeClassID'),
              fecha: $rootScope.comprar.fecha,
              horario: $rootScope.comprar.horario,
              profesor: $rootScope.comprar.profesor,
              alumno: $rootScope.comprar.alumno,
              direccion: $rootScope.direccionAgendar
            }).then(function () {

            });


            $rootScope.ok('Clase agendada con éxito', function () {
              $state.go('main.agenda');
            });
          }, function () {
            $rootScope.ok('Agenda de clase rechazada');
            $state.go('main.agenda');
          });
        }
        else {
          $rootScope.cargada = 1;
        }
        $rootScope.hideload();

      },
      function () {
        $rootScope.hideload();
        $rootScope.err();
      });

  }

  $scope.comprar = function (idplan) {
    $rootScope.showload();
    $http.post(Cfg.rest, {
      action: 'comprar_plan',
      user_id: localStorage.getItem('HomeClassID'),
      plan_id: idplan,
      fecha: ($stateParams.add == '1' ? '' : $rootScope.comprar.fecha),
      horario: ($stateParams.add == '1' ? '' : $rootScope.comprar.horario),
      profesor: ($stateParams.add == '1' ? '' : $rootScope.comprar.profesor),
      alumno: ($stateParams.add == '1' ? '' : $rootScope.comprar.alumno),
      direccion: $rootScope.direccionAgendar
    }).then(
        function (response, status, headers, config) {
          $rootScope.cargada = 1;
          if (response.data.res == 'ok') {


            var f = document.createElement('form');
            f.setAttribute('method', 'post');
            f.setAttribute('action', response.data.url);
            var i = document.createElement('input'); //input element, text
            i.setAttribute('type', 'hidden');
            i.setAttribute('name', 'token_ws');
            i.setAttribute('value', response.data.token);
            f.appendChild(i);
            document.body.appendChild(f);
            f.submit();


          }
          else {
            $rootScope.hideload();
            $rootScope.err(response.data.msg);
          }
        },
        function () {
          $rootScope.hideload();
          $rootScope.err();
        });
  };


    /*vm.requests = [{
      student: 'Juan Perez',
      signature: 'historia y geografía',
      area: 'Las Condes',
      level: '3° Medio',
      schedule: '6 PM - 8 PM'
    }, {
      student: 'Mariana Silva',
      signature: 'QUÍMICA',
      area: 'Providencia',
      level: '4° Medio',
      schedule: '1 PM - 4 PM'
    }, {
      student: 'Marcela Ubeda',
      signature: 'LENGUAJE BÁSICO',
      area: 'Providencia',
      level: '8° Básico',
      schedule: '5 PM - 7 PM'
    }];*/

});
