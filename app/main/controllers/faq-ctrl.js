'use strict';
angular.module('main')
.controller('FaqCtrl', function ($scope, $log, $http, $state, $ionicLoading, $rootScope, $sce, Cfg) {

  $scope.groups = [];

  $rootScope.showload();

  $http.post(Cfg.rest, {action: 'FAQ'}).then(
    function (response, status, headers, config) {
      $scope.groups = response.data;
      $rootScope.hideload();
    },
    function () {
      $rootScope.hideload();
      $rootScope.err();
    });

  $scope.toggleGroup = function (group) {
    if ($scope.isGroupShown(group)) {
      $scope.shownGroup = null;
    } else {
      $scope.shownGroup = group;
    }
  };
  $scope.html = function (html) {
    return $sce.trustAsHtml(html);
  };
  $scope.isGroupShown = function (group) {
    return $scope.shownGroup === group;
  };
});
///
