'use strict';
angular.module('main')
.controller('TeacherCtrl', function () {

  var vm = this;

  vm.maxRating = 5;
  vm.teacher = {
    id: 1,
    name: 'Juan Perez',
    avatar: 'main/assets/images/person1.png',
    lesson: 'Historia y Geografía',
    university: 'U. de Chile',
    rating: 5
  };

});
