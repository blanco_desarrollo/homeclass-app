'use strict';
angular.module('main')
  .controller('HistoryTeacherCtrl', function () {

    var vm = this;
    vm.histories = [{
      id: 1,
      plan: 'una hora',
      signature: 'historia y geografía',
      details: {
        status: 'Realizado',
        student: 'Juan Perez',
        date: '20 de enero 2017',
        rating: 5,
        percentage: 100,
        pay: '15.000'
      }
    }, {
      id: 2,
      plan: 'plan semestral',
      signature: 'matemáticas',
      details: {
        status: 'Realizado',
        student: 'Juan Perez',
        date: '20 de enero 2017',
        rating: 3,
        percentage: 60,
        pay: '15.000'
      }
    }];

    vm.maxRating = 5;

    vm.toggleGroup = function (id) {
      if (vm.isGroupShown(id)) {
        vm.shownGroup = null;
      } else {
        vm.shownGroup = id;
      }
    };
    vm.isGroupShown = function (id) {
      return vm.shownGroup === id;
    };

  });
