'use strict';
angular.module('main')
.controller('AgendaCtrl', function ($scope, $ionicModal, $log, $http, $state, $stateParams, $ionicLoading, $rootScope, $ionicNavBarDelegate, Cfg) {

  var vm = this;
  function nextMonth () { vm.calendarDate.setMonth(vm.calendarDate.getMonth() + 1); }

  function previousMonth () { vm.calendarDate.setMonth(vm.calendarDate.getMonth() - 1); }

  vm.calendarDate = new Date();
  vm.nextMonth = nextMonth;
  vm.previousMonth = previousMonth;

  $scope.nextMes = function () {
    vm.nextMonth();
    $scope.getInfo();
  };

  $scope.prevMes = function () {
    vm.previousMonth();
    $scope.getInfo();
  };
  $scope.sel = { horario: '', profesor: '', dia: '', mes: '', ano: '' };
  $scope.class = [];
  $scope.clases = 0;

  $scope.getInfo = function () {
    $rootScope.showload();

    $http.post(Cfg.rest, {action: 'getAgendaMe',
      id: localStorage.getItem('HomeClassID'),
      perfil: localStorage.getItem('HomeClassProfile'),
      mes: (vm.calendarDate.getMonth() + 1),
      ano: vm.calendarDate.getFullYear()}
    ).then(
      function (response, status, headers, config) {
        $scope.class = response.data.class;
        if (response.data.class) {
          $scope.clases = response.data.class.length;
        }
        else {
          $scope.clases = 0;
        }
        $rootScope.hideload();
      },
      function () {
        $rootScope.hideload();
        $rootScope.err();
      }
    );
  };

  $scope.getInfo();
  $scope.movement = [];
  $scope.modalClase = {};
  $scope.puedeReagendar = 0;
  if (localStorage.getItem('HomeClassProfile') != 'profesor') {
    $scope.puedeReagendar = 1;
  }

  $scope.closeReagendar = function () {
    $scope.seleccionReagendar.hide();
    setTimeout(function () { $scope.seleccionReagendar.remove(); }, 800);
  };
  $scope.reagendar_ok = function (clase, newHorarioId, newFecha) {
    $rootScope.confirmar('¿Confirmas el cambio de horario/dia?', function () {
      $rootScope.showload();
      $http.post(Cfg.rest, {action: 'reAgendarClase',
        clase_id: clase.id,
        horario_id: newHorarioId,
        fecha: newFecha,
        startBy: localStorage.getItem('HomeClassProfile'),
        start: localStorage.getItem('HomeClassID')
      }).then(
          function (response, status, headers, config) {
            $rootScope.hideload();
            $rootScope.ok('Reagendada con éxito', function () {
              $scope.seleccionReagendar.hide();
              setTimeout(function () { $scope.seleccionReagendar.remove(); }, 800);
              $scope.getInfo();
            });
          },
          function () {
            $rootScope.hideload();
            $rootScope.err();
          }
        );
    });

  };
  $scope.profesor_reagendar = function (agenda_id, clase) {
    if (localStorage.getItem('HomeClassProfile') != 'profesor') {

      $rootScope.showload();
      /* ALUMNO MUEVE LA CLASE */
      $http.post(Cfg.rest, {action: 'getDisponibilidadProfesor',
        id: clase.profesor_id,
        perfil: 'profesor',
        reagendar: agenda_id
      }).then(
        function (response, status, headers, config) {
          if (response.data.total == 0) {
            $rootScope.err('El profesor no tiene disponibilidad para mover esta clase');
          }
          else {
            $scope.modalClase = clase;
            $scope.movement = response.data.movement;
            $ionicModal.fromTemplateUrl('main/templates/reagendar.html', {
              scope: $scope
            }).then(function (modal) {
              $scope.seleccionReagendar = modal;
              $scope.seleccionReagendar.show();
            });
          }
          $rootScope.hideload();
        },
        function () {
          $rootScope.hideload();
          $rootScope.err();
        }
      );
    }
    else {

      /* PROFESOR QUIERE MOVER LA CLASE */
      $rootScope.showload();

      $http.post(Cfg.rest, {action: 'getDisponibilidadProfesor',
        id: localStorage.getItem('HomeClassID'),
        perfil: localStorage.getItem('HomeClassProfile'),
        reagendar: agenda_id
      }).then(
        function (response, status, headers, config) {
          $scope.class = response.data.class;
          if (response.data.total == 0) {
            $rootScope.err('No tienes disponibilidad futura para mover esta clase');
          }
          else {
            $scope.modalClase = clase;
            $scope.movement = response.data.movement;
            $ionicModal.fromTemplateUrl('main/templates/reagendar.html', {
              scope: $scope
            }).then(function (modal) {
              $scope.seleccionReagendar = modal;
              $scope.seleccionReagendar.show();
            });
          }
          $rootScope.hideload();
        },
        function () {
          $rootScope.hideload();
          $rootScope.err();
        }
      );

    }

  };
});
