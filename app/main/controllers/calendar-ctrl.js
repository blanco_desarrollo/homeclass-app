'use strict';
angular.module('main')
.controller('CalendarCtrl', function ($scope, $ionicModal, $log, $http, $state, $stateParams, $ionicLoading, $rootScope, $ionicNavBarDelegate, Cfg) {

  var vm = this;

  function nextMonth () {
    vm.calendarDate.setMonth(vm.calendarDate.getMonth() + 1);
    _refreshCalendar();
  }

  function previousMonth () {
    vm.calendarDate.setMonth(vm.calendarDate.getMonth() - 1);
    _refreshCalendar();
  }

  function _refreshCalendar (scheduledDays) {
    if (scheduledDays === undefined) {
      scheduledDays = [];
    }
    vm.calendarArray = _getMonthArray(vm.calendarDate.getMonth(), vm.calendarDate.getFullYear(), scheduledDays);
  }

  function _getMonthArray (month, year, scheduledDays) {
    var finalArray = [];

    if (scheduledDays === undefined) {
      scheduledDays = [];
    }

    var today = new Date();

    var firstDayDate = new Date(year, month, 1);
    var lastDayPreviousMonth = new Date(year, month, 0).getDate();
    var lastDay = new Date(year, month + 1, 0).getDate();

    var len = lastDay + firstDayDate.getDay() - 1;

    var rows = Math.ceil(len / 7);

    for (var i = 0; i < rows; i++) {
      finalArray[i] = [];
    }

    var startFrom = lastDayPreviousMonth - (firstDayDate.getDay() - 1);

    //for (i = 1; i < firstDayDate.getDay(); i++) {
    for (i = 1; i <= today.getDay(); i++) {
      finalArray[0][i - 1] = {
        day: startFrom + i,
        class: 'previous'
      };
    }

    var cssClass = 'current';

    var dayCount = 1;
    var nextMonthCount = false;
    for (var r = 0; r < rows; r++) {
      for (var c = 0; c < 7; c++) {
        if (dayCount > lastDay) {
          dayCount = 1;
          nextMonthCount = true;
          cssClass = 'next';
        }

        if (finalArray[r][c] === undefined) {
          finalArray[r][c] = {
            day: dayCount,
            class: cssClass
          };

          if (scheduledDays.indexOf(dayCount) > -1 && nextMonthCount === false) {
            finalArray[r][c].class = 'today'; //scheduled
          }
          /*
          if (dayCount === today.getDate() && today.getMonth() === month && today.getFullYear() === year) {
            finalArray[r][c].class = 'today';
          }
          */
          dayCount++;
        }
      }
    }

    return finalArray;
  }

  $scope.sel = { horario: '', profesor: '', dia: '', mes: '', ano: '', alumno: '' };
  $scope.profesorView = false;
  $scope.profesor = null;
  $scope.diaSeleccionado = null;
  $scope.mesSeleccionado = null;
  $scope.fechaSeleccionado = new Date();
  $scope.als = [];

  if ($stateParams.profesor != '') {
    $scope.profesorView = true;
    $scope.horarios_dia = [];

    $rootScope.showload();
    $http.post(Cfg.rest, {action: 'getAgenda', profesor: $stateParams.profesor, user_id: localStorage.getItem('HomeClassID')}).then(
      function (response, status, headers, config) {

        /* OJO CON ESTA WEA */
        var scheduledDays = response.data.ocupado;
        $scope.profesor = response.data.profesor;
        $scope.als = response.data.als;
        if (response.data.als != null) { $scope.sel.alumno = response.data.als[0].id; }

        var sc = [];
        if ($rootScope.busqueda.dia != 0) {
          sc = [$rootScope.busqueda.dia];

          vm.calendarDate = $rootScope.busqueda.fecha;
          $scope.fechaSeleccionado = $rootScope.busqueda.fecha;
          $scope.loadDia($rootScope.busqueda.dia, $rootScope.busqueda.fecha);
        }
        else {
          vm.calendarDate = new Date();
        }

        if ($rootScope.busqueda.horario != null && $rootScope.busqueda.horario != '') {
          $scope.sel.horario = $rootScope.busqueda.horarioid;
        }

        $rootScope.direccionAgendar = response.data.direccion;

        $rootScope.hideload();

      },
      function () {
        $rootScope.hideload();
        $rootScope.err();
      });
  }

  $scope.agendar = function () {
    var hoy = new Date();
    var dif = ( (hoy.getTime() - $scope.fechaSeleccionado.getTime()) / 1000);

    if (dif >= 86400) {
      $rootScope.err('Fecha no puede ser del pasado');
    }
    else if ($scope.sel.alumno == '') {
      $rootScope.err('Debe seleccionar alumno para agendar');
    }
    else if ($scope.sel.horario == '') {
      $rootScope.err('Debe seleccionar horario para agendar');
    }
    else {
      $rootScope.comprar = {
        fecha: $rootScope.formatDate($scope.fechaSeleccionado),
        horario: $scope.sel.horario,
        profesor: $stateParams.profesor,
        alumno: $scope.sel.alumno
      };
      $state.go('main.request');
    }
  };
  $scope.loadDia = function (dia, o) {

    $rootScope.showload();

    $http.post(Cfg.rest, {action: 'getDisponibleDia', profesor: $stateParams.profesor, dia: dia, mes: (o.getMonth() + 1), ano: o.getFullYear()}).then(
      function (response, status, headers, config) {

        var sc = [];
        sc[0] = dia;
        //console.log(vm.calendarDate);
        vm.calendarArray = _getMonthArray(vm.calendarDate.getMonth() + 1, vm.calendarDate.getFullYear(), sc);
        var nd = new Date(vm.calendarDate.getFullYear(), vm.calendarDate.getMonth(), dia);
        $scope.fechaSeleccionado = nd;

        if (response.data.disponible == null) {
          $rootScope.hideload();
          $rootScope.err('Uuups!! el profesor no tiene horarios disponibles en la fecha solicitada');
        }
        else {
          $rootScope.hideload();
          $scope.horarios_dia = response.data.disponible;
        }
        $scope.diaSeleccionado = response.data.diaSeleccionado;
        $scope.mesSeleccionado = response.data.mesSeleccionado;


      },
      function () {
        $rootScope.hideload();
        $rootScope.err();
      });
  };

});
