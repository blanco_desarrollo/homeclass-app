'use strict';
angular.module('main')
.controller('ResultsCtrl', function ($scope, $ionicModal, $log, $ionicHistory, $http, $state, $ionicLoading, $rootScope, Cfg) {

  $scope.goBack = function () {
    $ionicHistory.goBack();
  };

  $scope.seeCalendar = function (user_id) {
    $state.go('main.calendar', { profesor: user_id });
  };

  $scope.modalProfile = null;
  $scope.verPerfilModal = {};

  $scope.closeOpenedProfile = function () {
    $scope.modalProfile.hide();
    setTimeout(function () { $scope.modalProfile.remove(); }, 800);
  };

  $scope.seeProfile = function (id) {
    $scope.showload();

    $http.post(Cfg.rest, {action: 'getMe', profile: 'profesor', id: id, apoderadoRef: localStorage.getItem('HomeClassID')}).then(function (response) {
      $scope.verPerfilModal = response.data.me;
      console.log($scope.verPerfilModal);
      $scope.hideload();
      $ionicModal.fromTemplateUrl('main/templates/verperfil.html', {
        scope: $scope
      }).then(function (modal) {
        $scope.modalProfile = modal;
        $scope.modalProfile.show();
      });

    });
  };
  /*
  var vm = this;

  vm.maxRating = 5;

  vm.results = [{
    id: 1,
    name: 'Juan Perez',
    avatar: 'main/assets/images/person1.png',
    lesson: 'Historia y Geografía',
    university: 'U. de Chile',
    rating: 5,
    ratingQuantity: 20
  }, {
    id: 2,
    name: 'Rosario Silva',
    avatar: 'main/assets/images/person2.png',
    lesson: 'Matemáticas',
    university: 'U. Católica',
    rating: 3,
    ratingQuantity: 20
  }];
  */

});
