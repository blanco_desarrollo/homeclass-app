'use strict';
angular
  .module('main')
  .component('hamburger', {
    templateUrl: 'main/templates/components/hamburger.html',
    controllerAs: 'btn'
  });
