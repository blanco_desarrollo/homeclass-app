'use strict';
angular
  .module('main')
  .component('bell', {
    templateUrl: 'main/templates/components/bell.html',
    controllerAs: 'btn'
  });
