'use strict';
angular
  .module('main')
  .component('homeclassButton', {
    bindings: {
      text: '@'
    },
    templateUrl: 'main/templates/components/homeclass-button.html',
    controllerAs: 'btn'
  });
