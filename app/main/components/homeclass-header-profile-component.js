'use strict';
angular
  .module('main')
  .component('homeclassHeaderProfile', {
    bindings: {
      avatar: '@',
      cover: '@'
    },
    templateUrl: 'main/templates/components/homeclass-header-profile.html',
    controllerAs: 'header'
  });
