'use strict';
angular.module('main', [
  'ionic',
  'ngCordova',
  'ui.router',
  'angular-svg-round-progressbar',
  'angularTrix'
])
.config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

  // ROUTING with ui.router
  $ionicConfigProvider.views.transition('none');
  $ionicConfigProvider.views.maxCache(0);
  //$urlRouterProvider.otherwise('/welcome');
  $stateProvider
    // this state is placed in the <ion-nav-view> in the index.html
    .state('main', {
      url: '/main',
      abstract: true,
      templateUrl: 'main/templates/menu.html',
      controller: 'MenuCtrl as menu'
    })
    .state('main.dashboard', {
      url: '/dashboard',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/dashboard.html',
          controller: 'DashboardCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.exito', {
      url: '/exito/{trx:/?.*}',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/exito.html',
          controller: 'ExitoCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.rechazo', {
      url: '/rechazo/{trx:/?.*}',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/rechazo.html',
          controller: 'RechazoCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.profehome', {
      url: '/profesorHome',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/profehome.html',
          controller: 'ProfehomeCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.faq', {
      url: '/faq',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/faq.html',
          controller: 'FaqCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.add', {
      url: '/add',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/add.html',
          controller: 'AddCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.credits', {
      url: '/credits',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/credits.html',
          controller: 'CreditsCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.creditosprofe', {
      url: '/creditosprofe',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/creditosprofe.html',
          controller: 'CreditosprofeCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.search', {
      url: '/search',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/search.html',
          controller: 'SearchCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.results', {
      url: '/results',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/results.html',
          controller: 'ResultsCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.teacher', {
      url: '/teacher',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/teacher.html',
          controller: 'TeacherCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.summary', {
      url: '/summary',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/summary.html',
          controller: 'SummaryCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.schedule', {
      url: '/schedule',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/schedule.html',
          controller: 'ScheduleCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.panic', {
      url: '/panic',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/panic.html',
          controller: 'PanicCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.change', {
      url: '/change',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/change.html',
          controller: 'ChangeCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.profile', {
      url: '/profile',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/profile.html',
          controller: 'ProfileCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.calendar', {
      url: '/calendar',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/calendar.html',
          controller: 'CalendarCtrl as ctrl',
          cache: false
        }
      },
      params: {
        profesor: ''
      }
    })
    .state('main.agenda', {
      url: '/agenda',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/agenda.html',
          controller: 'AgendaCtrl as ctrl',
          cache: false
        }
      },
      params: {
        profesor: ''
      }
    })
    .state('main.history_tutor', {
      url: '/history/tutor',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/history-tutor.html',
          controller: 'HistoryTutorCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.history_teacher', {
      url: '/history/teacher',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/history-teacher.html',
          controller: 'HistoryTeacherCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.request', {
      url: '/request',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/request.html',
          controller: 'RequestCtrl as ctrl',
          cache: false
        }
      },
      params: {
        add: ''
      }
    })
    .state('main.evaluation', {
      url: '/evaluation',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/evaluation.html',
          controller: 'EvaluationCtrl as ctrl',
          cache: false
        }
      }
    })
    .state('main.control', {
      url: '/control',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/control.html',
          controller: 'ControlCtrl as ctrl',
          cache: false
        }
      }
    });
});
