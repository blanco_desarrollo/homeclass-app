'use strict';
angular.module('auth', [
  'ionic',
  'ngCordova',
  'ui.router',
  // TODO: load other modules selected during generation
])
.config(function ($stateProvider) {

  // ROUTING with ui.router
  $stateProvider
    .state('login', {
      url: '/login',
      templateUrl: 'auth/templates/login.html',
      controller: 'LoginCtrl as ctrl'
    })
    .state('welcome', {
      url: '/welcome',
      templateUrl: 'auth/templates/welcome.html',
      controller: 'WelcomeCtrl as ctrl'
    })
    .state('signup_tutor', {
      url: '/signup/tutor',
      templateUrl: 'auth/templates/signup-tutor.html',
      controller: 'SignupTutorCtrl as ctrl'
    })
    .state('signup_teacher', {
      url: '/signup/teacher',
      templateUrl: 'auth/templates/signup-teacher.html',
      controller: 'SignupTeacherCtrl as ctrl'
    })
  ;
});
