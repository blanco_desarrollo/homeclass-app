'use strict';
angular
  .module('auth')
  .component('homeclassHeader', {
    templateUrl: 'auth/templates/components/homeclass-header.html',
    controllerAs: 'head'
  });
