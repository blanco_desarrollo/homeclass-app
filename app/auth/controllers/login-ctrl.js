'use strict';
angular.module('auth')
.controller('LoginCtrl', function ($scope, $log, $http, $state, $ionicLoading, $rootScope, Cfg) {
	//alert('welcomeCTRL');
  $scope.login = {user: '', pass: ''};

  $scope.goLogin = function () {

	  /* hide onboarding */
    localStorage.setItem('onBoarding', 'true');

    if ($scope.login.user == '') {
      $rootScope.err('Debe ingresar un nombre de usuario');
    }
    else if ($scope.login.pass == '') {
      $rootScope.err('Debe ingresar una contraseña');
    }
    else {
      $rootScope.showload();
      $http.post(Cfg.rest, {action: 'login', user: $scope.login.user, pass: $scope.login.pass}).then(
				function (response, status, headers, config) {
  $rootScope.hideload();

  if (response.data.res == 'OK') {
    $rootScope.user_id = response.data.id;
    localStorage.setItem('HomeClassID', $rootScope.user_id);
    localStorage.setItem('HomeClassProfile', response.data.profile);

				      $http.post(Cfg.rest, {action: 'getNotifications',
				                            id: localStorage.getItem('HomeClassID'),
				                            perfil: localStorage.getItem('HomeClassProfile'),
				                           }).then(
				        function (response, status, headers, config) {
				          $rootScope.qtynotify = response.data.notifyUnread;
				          $rootScope.notify = response.data.notify;
				          $rootScope.minnotify = response.data.notify.length;
				        }
				      );


    if (response.data.profile == 'profesor') {

      if (window.cordova) {
        var push = PushNotification.init({
								  android: {
								    senderID: '874092342410'
								  },
								  ios: {
								    alert: 'true',
								    badge: true,
								    sound: 'false'
								  }
        });
        push.on('registration', function (data) {
								  $http.post(Cfg.rest, {
								        action: 'send_token',
								        token: data.registrationId,
								        tipo: 'profesor',
								        id: $rootScope.user_id,
								        os: cordova.platformId
								      }).then(
								  function (response, status, headers, config) {

								  },
								  function () {

								  });
        });
      }


      $state.go('main.profehome');
    }
    else {


      if (window.cordova) {
        var push = PushNotification.init({
								  android: {
								    senderID: '874092342410'
								  },
								  ios: {
								    alert: 'true',
								    badge: true,
								    sound: 'false'
								  }
        });
        push.on('registration', function (data) {
								  $http.post(Cfg.rest, {
								        action: 'send_token',
								        token: data.registrationId,
								        tipo: 'apoderado',
								        id: $rootScope.user_id,
								        os: cordova.platformId
								      }).then(
								  function (response, status, headers, config) {

								  },
								  function () {

								  });
        });
      }

      $state.go('main.search');
    }

  }
  else {
    $rootScope.err(response.data.msg);
  }
},
				function () {
  $rootScope.hideload();
  $rootScope.err();
});
    }
  };
});
