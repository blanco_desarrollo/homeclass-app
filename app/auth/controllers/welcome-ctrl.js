'use strict';
angular.module('auth')
.controller('WelcomeCtrl', function ($scope, $log, $http, $state, $ionicLoading, $rootScope, Cfg) {
	//alert('welcomeCTRL');
  $scope.login = {user: '', pass: ''};

  $scope.goLogin = function () {
    $state.go('login');
  };

  $scope.goSignup = function () {
    $state.go('signup_tutor');
  };

});
