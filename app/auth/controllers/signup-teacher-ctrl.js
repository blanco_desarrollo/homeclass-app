'use strict';
angular.module('auth')
.controller('SignupTeacherCtrl', function ($scope, $log, $http, $state, $ionicLoading, $rootScope, Cfg) {

  $scope.reg = { lugarestudio: '', empleo: '', area: '', horario: '', pass: '', nombre: '' };

  $scope.registrar = function () {
    if ($scope.reg.lugarestudio == '') {
      $rootScope.err('Debe ingresar su nombre de apoderado');
    }
    else if ($scope.reg.horario == '') {
      $rootScope.err('Debe ingresar al menos 1 bloque horario general de disponibilidad');
    }
    else if ($scope.reg.area == '') {
      $rootScope.err('Debe ingresar al menos 1 area de estudio');
    }
    else if ($scope.reg.pass == '') {
      $rootScope.err('Debe ingresar una clave/contraseña');
    }
    else if ($scope.reg.nombre == '') {
      $rootScope.err('Debe ingresar su nombre');
    }
    else {
      $rootScope.showload();
      $http.post(Cfg.rest, {action: 'reg_profe', nombre: $scope.reg.user, clave: $scope.reg.clave}).then(
				function (response, status, headers, config) {
  $rootScope.hideload();
  if (response.data.res == 'OK') {
    $rootScope.user_id = response.data.id;
    localStorage.setItem('HomeClassID', $rootScope.user_id);
    $rootScope.ok('Tu cuenta está lista. Bienvenido a HomeClass');
    $state.go('main.profile');
  }
  else {
    $rootScope.err(response.data.msg);
  }
},
				function () {
  $rootScope.hideload();
  $rootScope.err();
});
    }
  };

});

