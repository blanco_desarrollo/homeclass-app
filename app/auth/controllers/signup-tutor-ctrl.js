'use strict';
angular.module('auth')
.controller('SignupTutorCtrl', function ($scope, $log, $http, $state, $ionicLoading, $rootScope, Cfg) {

  $scope.reg = { apoderado: '', alumnos: [ {nombre: ''} ], email: '', clave: '', acepto: false };

  $scope.addAlumno = function () {
    var o = { nombre: '' };
    $scope.reg.alumnos.push(o);
  };

  $scope.registroApoderado = function () {

    if ($scope.reg.apoderado == '') {
      $rootScope.err('Debe ingresar su nombre de apoderado');
    }
    else if ($scope.reg.email == '') {
      $rootScope.err('Debe ingresar un email');
    }
    else if ($scope.reg.clave == '') {
      $rootScope.err('Debe ingresar una clave/contraseña');
    }
    else if ($scope.reg.acepto == false) {
      $rootScope.err('Debe aceptar nuestros términos para crear su cuenta');
    }
    else {
      $rootScope.showload();
      var alumnos = '';
      var alm = [];
      var error_alumno = 1;
      for (var i = 0;i < $scope.reg.alumnos.length;i++) {
        alm.push($scope.reg.alumnos[i].nombre);
        error_alumno = 0;
      }

      if (error_alumno == 0) {
        $http.post(Cfg.rest, {action: 'reg_apo', nombre: $scope.reg.apoderado, alumnos: alm.join(','), email: $scope.reg.email, clave: $scope.reg.clave}).then(
				function (response, status, headers, config) {
  $rootScope.hideload();
  if (response.data.res == 'OK') {
    $rootScope.user_id = response.data.id;
    localStorage.setItem('HomeClassID', $rootScope.user_id);
    $rootScope.ok('Tu cuenta está lista. Bienvenido a HomeClass');
    $state.go('main.search');
  }
  else {
    $rootScope.err(response.data.msg);
  }
},
				function () {
  $rootScope.hideload();
  $rootScope.err();
});
      } else {
        $rootScope.err('Debe ingresar a lo menos 1 alumno');
      }
    }
  };
  $scope.registroGoogle = function () {

	    $scope.showload();
	    window.plugins.googleplus.login(
	        {
	          'scopes': '', // optional, space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
	          'webClientId': '', // optional (client id of the web app/server side) clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
	          'offline': false, // optional, but requires the webClientId - if set to true the plugin will also return a serverAuthCode, which can be used to grant offline access to a non-Google server
	        },
	        function (obj) {

          var dataPost = {
            'username': obj.email,
            'email': obj.email,
            'googleId': obj.userId,
            'avatar': obj.imageUrl,
            'firstName': obj.displayName,
            'social': 1
          };

          $scope.reg.apoderado = obj.displayName;
          $scope.reg.email = obj.email;
          $scope.hideload();
        }, function (err) {
          $rootScope.err(err);
          $scope.hideload();
        });
	    $scope.showload();
  };

  $scope.registroFacebook = function () {

	   $rootScope.showload();

	    CordovaFacebook.login({
	       permissions: ['email', 'public_profile'],
	       onSuccess: function (result) {
	          if (result.declined.length > 0) {
	             $rootScope.err('Se ha rechazado la conexión con Facebook');
	             $rootScope.hideload();
	          }
	          else if (result.success == 1) {

	            $http.get('https://graph.facebook.com/me?fields=id,name,email,picture&access_token=' + result.accessToken, {})
	            .then(function (data, status, headers, config) {

	                var dataPost = {
	                  'username': data.data.email,
	                  'firstName': data.data.name,
	                  'email': data.data.email,
	                  'facebookId': data.data.id,
	                  'picture': data.data.picture.data.url
	                };

	                $scope.reg.apoderado = data.data.name;
              $scope.reg.email = data.data.email;
              $rootScope.hideload();

	            }, function () {
	              $rootScope.hideload();
	              $rootScope.err('No fue posible obtener la información desde Facebook');
	            });

	          }
	          else {
	            $rootScope.hideload();
	            $rootScope.err('Error al intentar conectar con Facebook. Intente más tarde');
	          }
	       },
	       onFailure: function (result) {
	          $rootScope.hideload();
	          if (result.cancelled) {
	             $rootScope.err('No has autorizado Homeclass en tu cuenta de Facebook');
	          } else if (result.error) {
	             $rootScope.err('Bloqueaste a Homeclass para usar tu Facebook. Debes usar otro método de autentificación (' + result.errorLocalized + ')');
	          }
	       }
	    });


  };
});
