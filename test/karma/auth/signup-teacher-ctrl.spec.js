'use strict';

describe('module: auth, controller: SignupTeacherCtrl', function () {

  // load the controller's module
  beforeEach(module('auth'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var SignupTeacherCtrl;
  beforeEach(inject(function ($controller) {
    SignupTeacherCtrl = $controller('SignupTeacherCtrl');
  }));

  it('should do something', function () {
    expect(!!SignupTeacherCtrl).toBe(true);
  });

});
