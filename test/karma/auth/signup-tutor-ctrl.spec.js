'use strict';

describe('module: auth, controller: SignupTutorCtrl', function () {

  // load the controller's module
  beforeEach(module('auth'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var SignupTutorCtrl;
  beforeEach(inject(function ($controller) {
    SignupTutorCtrl = $controller('SignupTutorCtrl');
  }));

  it('should do something', function () {
    expect(!!SignupTutorCtrl).toBe(true);
  });

});
