'use strict';

describe('module: main, controller: ResultsCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var ResultsCtrl;
  beforeEach(inject(function ($controller) {
    ResultsCtrl = $controller('ResultsCtrl');
  }));

  it('should do something', function () {
    expect(!!ResultsCtrl).toBe(true);
  });

});
