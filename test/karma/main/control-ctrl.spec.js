'use strict';

describe('module: main, controller: ControlCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var ControlCtrl;
  beforeEach(inject(function ($controller) {
    ControlCtrl = $controller('ControlCtrl');
  }));

  it('should do something', function () {
    expect(!!ControlCtrl).toBe(true);
  });

});
