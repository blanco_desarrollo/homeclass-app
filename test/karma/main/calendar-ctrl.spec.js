'use strict';

describe('module: main, controller: CalendarCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var CalendarCtrl;
  beforeEach(inject(function ($controller) {
    CalendarCtrl = $controller('CalendarCtrl');
  }));

  it('should do something', function () {
    expect(!!CalendarCtrl).toBe(true);
  });

});
