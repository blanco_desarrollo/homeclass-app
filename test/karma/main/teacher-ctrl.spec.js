'use strict';

describe('module: main, controller: TeacherCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var TeacherCtrl;
  beforeEach(inject(function ($controller) {
    TeacherCtrl = $controller('TeacherCtrl');
  }));

  it('should do something', function () {
    expect(!!TeacherCtrl).toBe(true);
  });

});
