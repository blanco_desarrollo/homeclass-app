'use strict';

describe('module: main, controller: HistoryTutorCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var HistoryTutorCtrl;
  beforeEach(inject(function ($controller) {
    HistoryTutorCtrl = $controller('HistoryTutorCtrl');
  }));

  it('should do something', function () {
    expect(!!HistoryTutorCtrl).toBe(true);
  });

});
