'use strict';

describe('module: main, controller: ScheduleCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var ScheduleCtrl;
  beforeEach(inject(function ($controller) {
    ScheduleCtrl = $controller('ScheduleCtrl');
  }));

  it('should do something', function () {
    expect(!!ScheduleCtrl).toBe(true);
  });

});
