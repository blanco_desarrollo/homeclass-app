'use strict';

describe('module: main, controller: PanicCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var PanicCtrl;
  beforeEach(inject(function ($controller) {
    PanicCtrl = $controller('PanicCtrl');
  }));

  it('should do something', function () {
    expect(!!PanicCtrl).toBe(true);
  });

});
