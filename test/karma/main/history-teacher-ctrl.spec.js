'use strict';

describe('module: main, controller: HistoryTeacherCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var HistoryTeacherCtrl;
  beforeEach(inject(function ($controller) {
    HistoryTeacherCtrl = $controller('HistoryTeacherCtrl');
  }));

  it('should do something', function () {
    expect(!!HistoryTeacherCtrl).toBe(true);
  });

});
