'use strict';

describe('module: main, controller: EvaluationCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var EvaluationCtrl;
  beforeEach(inject(function ($controller) {
    EvaluationCtrl = $controller('EvaluationCtrl');
  }));

  it('should do something', function () {
    expect(!!EvaluationCtrl).toBe(true);
  });

});
