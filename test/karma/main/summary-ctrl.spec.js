'use strict';

describe('module: main, controller: SummaryCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var SummaryCtrl;
  beforeEach(inject(function ($controller) {
    SummaryCtrl = $controller('SummaryCtrl');
  }));

  it('should do something', function () {
    expect(!!SummaryCtrl).toBe(true);
  });

});
