'use strict';

describe('module: main, controller: RequestCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var RequestCtrl;
  beforeEach(inject(function ($controller) {
    RequestCtrl = $controller('RequestCtrl');
  }));

  it('should do something', function () {
    expect(!!RequestCtrl).toBe(true);
  });

});
